package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by Jack on 12/11/2016.
 */
public class PasswordChangeMessageSingleton extends Stage{

    private Label label1;
    private Label label2;
    private Button button1;
    private Button cancelButton;
    private Label title;
    private GridPane grid;
    private PasswordField pwFieldOld;
    private PasswordField pwFieldNew;
    private HBox buttons;
    private String passwordOld;
    private String passwordNew;
    private boolean cancelClicked;
    private static PasswordChangeMessageSingleton singleton = null;

    private PasswordChangeMessageSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static PasswordChangeMessageSingleton getSingleton() {
        if (singleton == null)
            singleton = new PasswordChangeMessageSingleton();
        return singleton;
    }

    private void setMessageTitle(String title){
        this.title.setText(title);
    }

    private void setLabels(String label1, String label2){
        this.label1.setText(label1);
        this.label2.setText(label2);
    }

    private void setButtons(String button1){
        this.button1.setText(button1);
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        //initStyle(StageStyle.UNDECORATED);
        initOwner(owner);
        initStyle(StageStyle.TRANSPARENT);

        grid = new GridPane();
        grid.setVgap(10);

        grid.setMinWidth(250);
        grid.setMinHeight(250);

        title = new Label();
        label1 = new Label();
        label2 = new Label();

        pwFieldOld = new PasswordField();
        pwFieldNew = new PasswordField();

        button1 = new Button();
        cancelButton = new Button("Cancel");

        buttons = new HBox(20);
        buttons.getChildren().addAll(button1,cancelButton);

        grid.add(title,0,0);
        grid.add(label1,0,1);
        grid.add(pwFieldOld,0,2);
        grid.add(label2,0,3);
        grid.add(pwFieldNew,0,4);
        grid.add(buttons,0,5);

        grid.setAlignment(Pos.CENTER);

        initStyles();
        initEventHandlers();

        Scene textFieldsAndButtonsScene = new Scene(grid,250,250);
        textFieldsAndButtonsScene.setFill(Color.TRANSPARENT);
        this.setScene(textFieldsAndButtonsScene);
    }

    private void initStyles(){
        grid.setStyle(
                "-fx-background-color: rgba(0, 0, 0, 0.6);"
        );

        title.setTextFill(Color.rgb(0,153,92));
        title.setFont(new Font("Tahoma",25));

        label1.setTextFill(Color.rgb(0,153,92));
        label2.setTextFill(Color.rgb(0,153,92));

        pwFieldOld.setStyle(
                "-fx-background-color: rgba(40,40,40,0.8);" +
                        "-fx-text-fill: rgb(0,153,92);"
        );

        pwFieldNew.setStyle(
                "-fx-background-color: rgba(40,40,40,0.8);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        button1.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        cancelButton.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        buttons.setPadding(new Insets(0,30,0,30));
    }

    private void initEventHandlers(){
        cancelButton.setOnMouseClicked(e->{
            cancelClicked = true;
            this.close();
        });

        button1.setOnMouseClicked(e->{
            cancelClicked = false;
            setTextFields();
            this.close();
        });
    }

    public String getOption(){
        return cancelClicked?"":button1.getText();
    }

    private void clearFields(){
        pwFieldOld.clear();
        pwFieldNew.clear();
    }

    //RETURNS IF VALID USERNAME AND PASSWORD
    private void setTextFields(){
        passwordOld = pwFieldOld.getText();
        passwordNew = pwFieldNew.getText();
    }

    public String getOldPassword(){
        return passwordOld;
    }

    public String getNewPassword(){
        return passwordNew;
    }

    public boolean cancelClicked(){ return cancelClicked;}

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     */
    public void show(String title, String label1, String label2, String button1) {
        clearFields();
        setMessageTitle(title);
        setLabels(label1, label2); // message displayed to the user
        setButtons(button1);
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }
}
