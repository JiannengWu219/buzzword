package ui;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by Jack on 11/14/2016.
 */
public class FourButtonsCancelDialogSingleton extends Stage {

    private Label titleLabel;
    private Button cancelButton;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private GridPane grid;
    private String option;

    private static FourButtonsCancelDialogSingleton singleton = null;

    private FourButtonsCancelDialogSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static FourButtonsCancelDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new FourButtonsCancelDialogSingleton();
        return singleton;
    }

    private void initMessageTitle(String title){
        this.titleLabel.setText(title);
    }

    private void initButtons(String button1, String button2, String button3, String button4){
        this.button1.setText(button1);
        this.button2.setText(button2);
        this.button3.setText(button3);
        this.button4.setText(button4);
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        //initStyle(StageStyle.UNDECORATED);
        initOwner(owner);
        initStyle(StageStyle.TRANSPARENT);

        grid  = new GridPane();
        grid.setVgap(10);

        grid.setMinWidth(250);
        grid.setMinHeight(275);

        titleLabel = new Label();
        grid.setHalignment(titleLabel, HPos.CENTER);

        button1 = new Button();
        button2 = new Button();
        button3 = new Button();
        button4 = new Button();

        cancelButton = new Button("Cancel");
        grid.setHalignment(cancelButton, HPos.CENTER);


        //FIXME WITH CSS
        button1.setMinWidth(110);
        button2.setMinWidth(110);
        button3.setMinWidth(110);
        button4.setMinWidth(110);
        cancelButton.setMinWidth(75);

        grid.add(titleLabel,0,0);
        grid.add(button1,0,1);
        grid.add(button2,0,2);
        grid.add(button3,0,3);
        grid.add(button4,0,4);
        grid.add(cancelButton,0,5);

        grid.setAlignment(Pos.CENTER);

        initStyle();
        initEventHandlers();

        Scene textFieldsAndButtonsScene = new Scene(grid,250,250);
        textFieldsAndButtonsScene.setFill(Color.TRANSPARENT);
        this.setScene(textFieldsAndButtonsScene);
    }

    private void initStyle(){
        String style = "-fx-background-color: rgba(40,40,40,0.9);"+
                "-fx-text-fill: rgb(0,153,92);"+
                "-fx-font-size: 13;";

        grid.setStyle(
                "-fx-background-color: rgba(0, 0, 0, 0.6);"
        );

        titleLabel.setTextFill(Color.rgb(0,153,92));
        titleLabel.setFont(new Font("Tahoma",20));

        button1.setStyle(style);
        button2.setStyle(style);
        button3.setStyle(style);
        button4.setStyle(style);
        cancelButton.setStyle(style);
    }

    private void initEventHandlers(){
        button1.setOnMouseClicked(e->{
            option = button1.getText();
            this.close();
        });

        button2.setOnMouseClicked(e->{
            option = button2.getText();
            this.close();
        });

        button3.setOnMouseClicked(e->{
            option = button3.getText();
            this.close();
        });

        button4.setOnMouseClicked(e->{
            option = button4.getText();
            this.close();
        });
        cancelButton.setOnMouseClicked(e->{
            this.close();
        });
    }

    public String getOption(){
        return option;
    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     */
    public void show(String title, String button1, String button2, String button3, String button4) {
        initMessageTitle(title);
        initButtons(button1, button2, button3, button4);
        option = "";
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }
}
