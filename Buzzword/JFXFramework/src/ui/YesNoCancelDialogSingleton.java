package ui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @author Ritwik Banerjee
 */
public class YesNoCancelDialogSingleton extends Stage {
    // HERE'S THE SINGLETON
    static YesNoCancelDialogSingleton singleton;

    // GUI CONTROLS FOR OUR DIALOG
    VBox   messagePane;
    Scene  messageScene;
    Label  messageLabel;
    Button yesButton;
    Button noButton;
    Button cancelButton;
    String selection;
    HBox buttonBox;

    // CONSTANT CHOICES
    public static final String YES    = "Yes";
    public static final String NO     = "No";
    public static final String CANCEL = "Cancel";

    /**
     * Note that the constructor is private since it follows
     * the singleton design pattern.
     */
    private YesNoCancelDialogSingleton() {}

    /**
     * The static accessor method for this singleton.
     *
     * @return The singleton object for this type.
     */
    public static YesNoCancelDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new YesNoCancelDialogSingleton();
        return singleton;
    }

    /**
     * This method initializes the singleton for use.
     *
     * @param primaryStage The window above which this dialog will be centered.
     */
    public void init(Stage primaryStage) {
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        initStyle(StageStyle.TRANSPARENT);

        // LABEL TO DISPLAY THE CUSTOM MESSAGE
        messageLabel = new Label();

        // YES, NO, AND CANCEL BUTTONS
        yesButton = new Button(YES);
        noButton = new Button(NO);
        cancelButton = new Button(CANCEL);

        // MAKE THE EVENT HANDLER FOR THESE BUTTONS
        EventHandler<ActionEvent> yesNoCancelHandler = event -> {
            YesNoCancelDialogSingleton.this.selection = ((Button) event.getSource()).getText();
            YesNoCancelDialogSingleton.this.hide();
        };


        // AND THEN REGISTER THEM TO RESPOND TO INTERACTIONS
        yesButton.setOnAction(yesNoCancelHandler);
        noButton.setOnAction(yesNoCancelHandler);
        cancelButton.setOnAction(yesNoCancelHandler);

        // NOW ORGANIZE OUR BUTTONS
        buttonBox = new HBox(20);
        buttonBox.getChildren().add(yesButton);
        buttonBox.getChildren().add(noButton);
        buttonBox.getChildren().add(cancelButton);
        buttonBox.setAlignment(Pos.CENTER);

        // WE'LL PUT EVERYTHING HERE
        messagePane = new VBox();
        messagePane.setAlignment(Pos.CENTER);
        messagePane.getChildren().add(messageLabel);
        messagePane.getChildren().add(buttonBox);

        initStyles();

        // AND PUT IT IN THE WINDOW
        messageScene = new Scene(messagePane);
        messageScene.setFill(Color.TRANSPARENT);
        this.setScene(messageScene);
    }

    private void initStyles(){
        messagePane.setStyle(
                "-fx-min-width: 550;"+
                        "-fx-min-height: 200;"+
                "-fx-background-color: rgba(0, 0, 0, 0.6);"
        );

        messageLabel.setTextFill(Color.rgb(0,153,92));
        messageLabel.setFont(new Font("Tahoma",18));


        yesButton.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        noButton.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                    "-fx-text-fill: rgb(0,153,92);"
        );

        cancelButton.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        buttonBox.setPadding(new Insets(10,20,10,20));
    }

    /**
     * Accessor method for getting the selection the user made.
     *
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }

    /**
     * This method loads a custom message into the label
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window bar.
     * @param message Message to appear inside the dialog.
     */
    public void show(String title, String message) {
        // SET THE DIALOG TITLE BAR TITLE
        setTitle(title);

        // SET THE MESSAGE TO DISPLAY TO THE USER
        messageLabel.setText(message);

        // AND OPEN UP THIS DIALOG, MAKING SURE THE APPLICATION
        // WAITS FOR IT TO BE RESOLVED BEFORE LETTING THE USER
        // DO MORE WORK.
        showAndWait();
    }
    
}
