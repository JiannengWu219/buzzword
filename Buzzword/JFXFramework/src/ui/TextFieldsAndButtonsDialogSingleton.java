package ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import settings.InitializationParameters;
import sun.security.util.Password;

import static settings.AppPropertyType.SAVE_ERROR_MESSAGE;
import static settings.AppPropertyType.SAVE_ERROR_TITLE;

/**
 * Created by Jack on 11/14/2016.
 */
public class TextFieldsAndButtonsDialogSingleton extends Stage{

    private Label label1;
    private Label label2;
    private Button button1;
    private Button cancelButton;
    private Label title;
    private GridPane grid;
    private TextField usernameField;
    private PasswordField pwField;
    private HBox buttons;
    private String username;
    private String password;
    private boolean cancelClicked;
    private boolean goodPasswordAndUser;

    private static TextFieldsAndButtonsDialogSingleton singleton = null;

    private TextFieldsAndButtonsDialogSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static TextFieldsAndButtonsDialogSingleton getSingleton() {
        if (singleton == null)
            singleton = new TextFieldsAndButtonsDialogSingleton();
        return singleton;
    }

    private void setMessageTitle(String title){
        this.title.setText(title);
    }

    private void setLabels(String label1, String label2){
        this.label1.setText(label1);
        this.label2.setText(label2);
    }

    private void setButtons(String button1){
        this.button1.setText(button1);
    }

    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        //initStyle(StageStyle.UNDECORATED);
        initOwner(owner);
        initStyle(StageStyle.TRANSPARENT);

        grid = new GridPane();
        grid.setVgap(10);

        grid.setMinWidth(250);
        grid.setMinHeight(250);

        title = new Label();
        label1 = new Label();
        label2 = new Label();

        usernameField = new TextField();
        pwField = new PasswordField();

        button1 = new Button();
        cancelButton = new Button("Cancel");

        buttons = new HBox(20);
        buttons.getChildren().addAll(button1,cancelButton);

        grid.add(title,0,0);
        grid.add(label1,0,1);
        grid.add(usernameField,0,2);
        grid.add(label2,0,3);
        grid.add(pwField,0,4);
        grid.add(buttons,0,5);

        grid.setAlignment(Pos.CENTER);

        initStyles();
        initEventHandlers();

        Scene textFieldsAndButtonsScene = new Scene(grid,250,250);
        textFieldsAndButtonsScene.setFill(Color.TRANSPARENT);
        this.setScene(textFieldsAndButtonsScene);
    }

    private void initStyles(){
        grid.setStyle(
                "-fx-background-color: rgba(0, 0, 0, 0.6);"
        );

        title.setTextFill(Color.rgb(0,153,92));
        title.setFont(new Font("Tahoma",25));

        label1.setTextFill(Color.rgb(0,153,92));
        label2.setTextFill(Color.rgb(0,153,92));

        usernameField.setStyle(
                "-fx-background-color: rgba(40,40,40,0.8);" +
                        "-fx-text-fill: rgb(0,153,92);"
        );

        pwField.setStyle(
                "-fx-background-color: rgba(40,40,40,0.8);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        button1.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        cancelButton.setStyle(
                "-fx-background-color: rgba(40,40,40,0.9);"+
                        "-fx-text-fill: rgb(0,153,92);"
        );

        buttons.setPadding(new Insets(0,20,0,20));
    }

    private void initEventHandlers(){
        cancelButton.setOnMouseClicked(e->{
            cancelClicked = true;
            this.close();
        });

        button1.setOnMouseClicked(e->{
            cancelClicked = false;
            goodPasswordAndUser = setTextFields();
            this.close();
        });
    }

    private void clearFields(){
        usernameField.clear();
        pwField.clear();
    }

    //RETURNS IF VALID USERNAME AND PASSWORD
    private boolean setTextFields(){
        username = usernameField.getText();
        password = pwField.getText();

        return true;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public boolean cancelClicked(){ return cancelClicked;}

    public boolean isGoodPasswordAndUser(){return goodPasswordAndUser;}

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     */
    public void show(String title, String label1, String label2, String button1) {
        clearFields();
        setMessageTitle(title);
        setLabels(label1, label2); // message displayed to the user
        setButtons(button1);
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }
}
