package formatter;

import javafx.scene.Node;
import javafx.scene.layout.*;

/**
 * Created by Jack on 11/16/2016.
 */
public class JFXFormatter {

    public static HBox format(Positions position, Node content){
        //create container for the content
        HBox returnBox = new HBox();

        //generate fill boxes
        HBox leftBlankBox = new HBox();
        HBox rightBlankBox = new HBox();

        //fill boxes to max size of page
        HBox.setHgrow(leftBlankBox, Priority.ALWAYS);
        HBox.setHgrow(rightBlankBox, Priority.ALWAYS);

        //add boxes + content in proper manner
        if (position.equals(Positions.HORR_CENTER))
            returnBox.getChildren().addAll(leftBlankBox, content, rightBlankBox);
        else if (position.equals(Positions.FLOAT_LEFT))
            returnBox.getChildren().addAll(content, rightBlankBox);
        else if (position.equals(Positions.FLOAT_RIGHT))
            returnBox.getChildren().addAll(leftBlankBox, content);
        else
            throw new IllegalArgumentException("Invalid position type");

        //return box
        return returnBox;
    }

    public static HBox getGrowBox(){
        HBox growBox = new HBox();
        HBox.setHgrow(growBox,Priority.ALWAYS);
        return growBox;
    }
}
