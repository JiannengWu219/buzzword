package formatter;

/**
 * Created by Jack on 11/16/2016.
 */
public enum Positions {
    FLOAT_LEFT("FLOAT_LEFT"),
    FLOAT_RIGHT("FLOAT_RIGHT"),
    HORR_CENTER("HORR_CENTER");

    private String parameter;

    Positions(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}
