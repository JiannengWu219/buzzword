package gridgenerator;

import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Jack on 11/12/2016.
 */
public class GridGenerator {
    private final static String MAX_LENGTH           = "MAX_LENGTH";
    private final static String MAX_HEIGHT           = "MAX_HEIGHT";

    private final static String HORIZONTAL_CONNECTOR = "HORIZONTAL_CONNECTOR";

    //Information about data
    private DataGrid    data;
    private int         numRow;
    private int         numCol;

    //Information about the grid
    private float       length;     //desired length of grid in pixels
    private float       height;     //desired height of grid in pixels
    private String      limitation; //limitation for width/height based on length and height + row and col
    private GridPane    grid;       //the visual grid
    private double      radius;     //radius of each circle, all spacing and shapes will be based off this

    //Each node and it's locked state
    private HashMap<String,Boolean> dataAndLockedState;

    //All Nodes in Grid
    private ArrayList<VisualCircleNode> visualCircles;
    private ArrayList<VisualConnectorNode> horrizontalVisualConnectors;
    private ArrayList<VisualConnectorNode> verticalVisualConnectors;
    private VisualCircleNode currentCircleNode; //for mouse dragging
    private VisualCircleNode previousCircleNode; //for mouse dragging
    private ArrayList<VisualCircleNode> currentCircleNodes;
    private ArrayList<VisualCircleNode> previousCircleNodes;
    private Stack<VisualConnectorNode> previousConnections;
    private boolean first;

    //current user input
    private ArrayList<String> currentInput;

    //shadows
    private DropShadow circleDropShadow;
    private DropShadow highLightDropShadow;
    private DropShadow semiHighlightDropShadow;

    private boolean dragging;
    private boolean draggable;

    public GridGenerator(DataGrid data, float length, float height){
        this.data   = data;
        this.length = length;
        this.height = height;
        this.numRow = data.getRow();
        this.numCol = data.getCol();

        initGrid();
    }

    private void initGrid(){
        setLimitation();
        setRadius();
        initDropShadows();
        setGrid();
        initCircleEventHandlers();
        initConnectorEventHandlers();
    }

    //sets limitation based on the num of rows, and cols and desired width and height
    private void setLimitation(){
        float lenToColRatio     = length/numCol;
        float heightToRowRatio  = height/numRow;

        if(lenToColRatio <= heightToRowRatio)
            limitation = MAX_LENGTH;
        else
            limitation = MAX_HEIGHT;
    }

    private void setRadius(){
        int numNodesInLimit;
        double numRadii;

        if(limitation.equals(MAX_LENGTH))
            numNodesInLimit = numCol;
        else
            numNodesInLimit = numRow;

        numRadii = (numNodesInLimit+2)/10;  //spacing for gaps between each row/col
        numRadii += numNodesInLimit*2;      //spacing for the circles themselves
        numRadii += numNodesInLimit-1;      //spacing for rectangles between the circles

        if(limitation.equals(MAX_HEIGHT))
            radius = height/numRadii;
        else
            radius = length/numRadii;
    }

    private void setGrid(){
        grid = new GridPane();

        dataAndLockedState = new HashMap<>();
        currentInput = new ArrayList<>();
        visualCircles = new ArrayList<>();
        horrizontalVisualConnectors = new ArrayList<>();
        verticalVisualConnectors = new ArrayList<>();
        previousConnections = new Stack<>();
        currentCircleNodes = new ArrayList<>();
        previousCircleNodes = new ArrayList<>();

        int currentIndex = 0;
        int connectorRowHelperCounter = 0;

        for(int row = 1; row<=numRow*2-1; row++) {
            for (int col = 1; col <= numCol * 2 - 1; col++) {
                StackPane currentNode;
                if (row % 2 != 0) {
                    if (col % 2 != 0) { //currently a circle with data
                        String currentData = data.getData(currentIndex);
                        VisualCircleNode currentVisualCircle = new VisualCircleNode(radius, currentData, circleDropShadow, highLightDropShadow, semiHighlightDropShadow);
                        visualCircles.add(currentVisualCircle);
                        int currVisualIndex = visualCircles.indexOf(currentVisualCircle);
                        currentNode = currentVisualCircle.getNode();
                        currentIndex++;

                        if (col >= 3) {
                            VisualConnectorNode leftVisualConnector = horrizontalVisualConnectors.get(currVisualIndex - row + connectorRowHelperCounter);
                            currentVisualCircle.setLeftConnector(leftVisualConnector);
                            leftVisualConnector.setRightCirc(currentVisualCircle);
                        }
                        if (row > 1){
                            VisualConnectorNode topVisualConnector = verticalVisualConnectors.get(currVisualIndex-numCol);
                            topVisualConnector.setBottomCirc(currentVisualCircle);
                            currentVisualCircle.setTopConnector(topVisualConnector);
                        }
                    } else { //currently rendering a horizontal connector
                        VisualConnectorNode currentVisualConnector = new VisualConnectorNode(radius, HORIZONTAL_CONNECTOR);
                        horrizontalVisualConnectors.add(currentVisualConnector);
                        int currVisualIndex = horrizontalVisualConnectors.indexOf(currentVisualConnector);
                        currentNode = currentVisualConnector.getNode();

                        if (col >= 2) {
                            VisualCircleNode leftVisualCircle = visualCircles.get(currVisualIndex + connectorRowHelperCounter);
                            currentVisualConnector.setLeftCirc(leftVisualCircle);
                            leftVisualCircle.setRightConnector(currentVisualConnector);
                        }
                    }
                } else if (col % 2 != 0) { //currently a vertical connector
                    VisualConnectorNode currentVisualConnector = new VisualConnectorNode(radius, "VERTICAL");
                    verticalVisualConnectors.add(currentVisualConnector);
                    int currVisualIndex = verticalVisualConnectors.indexOf(currentVisualConnector);
                    currentNode = currentVisualConnector.getNode();

                    VisualCircleNode topVisualCircle = visualCircles.get(currVisualIndex);
                    currentVisualConnector.setTopCirc(topVisualCircle);
                    topVisualCircle.setBottomConnector(currentVisualConnector);
                } else {
                    currentNode = new StackPane();
                }
                GridPane.setConstraints(currentNode, col, row);
                grid.getChildren().add(currentNode);
            }
            if(row % 2 == 0){
                connectorRowHelperCounter ++;
            }
        }
    }

    private void initDropShadows(){
        //FIXME colors for dropshadow
        circleDropShadow = new DropShadow();
        circleDropShadow.setRadius(radius*0.8);
        circleDropShadow.setOffsetX(0);
        circleDropShadow.setOffsetY(0);
        circleDropShadow.setColor(Color.rgb(0,0,0,0.5));

        semiHighlightDropShadow = new DropShadow();
        semiHighlightDropShadow.setRadius(radius*1.2);
        semiHighlightDropShadow.setOffsetX(0);
        semiHighlightDropShadow.setOffsetY(0);
        semiHighlightDropShadow.setColor(Color.rgb(0,204,60,0.6));

        highLightDropShadow = new DropShadow();
        highLightDropShadow.setRadius(radius*1.2);
        highLightDropShadow.setOffsetX(0);
        highLightDropShadow.setOffsetY(0);
        highLightDropShadow.setColor(Color.rgb(0,255,75,0.8));
    }

    private void initCircleEventHandlers(){
        draggable = true;
        dragging = false;
        first = true;

        //THE ACTIONS FOR THE CIRCLES TO HIGHLIGHT/ HOVER
        for(VisualCircleNode VC: visualCircles){
            //get the data, used primarily by level select
            VC.getActionListner().setOnMouseClicked(e->{
                currentInput.clear();
                currentInput.add(VC.getData());
                if(draggable)
                    currentInput.clear();
            });

            //highlight circle properly when entered (non dragged)
            VC.getActionListner().setOnMouseEntered(e-> {
                if (!VC.isLocked()) {
                    if (!VC.isHighlighted())
                        VC.highlight(semiHighlightDropShadow);
                }
            });

            //unhighlightCircle properly after mouse leaves
            VC.getActionListner().setOnMouseExited(e->{
                if(!VC.isLocked()) {
                    if (dragging) {
                        VC.highlight(highLightDropShadow);
                    } else
                        VC.highlight(circleDropShadow);
                }
            });

            //show the highlights/options when mouse drag not yet dettected
            VC.getActionListner().setOnMousePressed(e->{
                if(!VC.isLocked()){
                    VC.highlight(highLightDropShadow);
                    if(draggable) {
                        currentInput.clear();
                        currentInput.add(VC.getData());
                        showOptions(VC);
                    }
                }
            });

            //unhighlight node that was pressed
            VC.getActionListner().setOnMouseReleased(e->{
                if(!VC.isLocked()){
                    unHighlightAround(VC);
                    currentInput.clear();
                }
            });

            //start detecting drag
            VC.getActionListner().setOnDragDetected(e->{
                if(!VC.isLocked()) {
                    if(draggable) {
                        VC.getNode().startFullDrag();
                        dragging = true;
                        currentInput.clear();
                    }
                }
            });

            //when entering a new node
            VC.getActionListner().setOnMouseDragEntered(e->{
                selectCurrentNode(VC);
            });
        }

        //FIXME MOVE ME
        //when released, reset back to original
        grid.setOnMouseDragReleased(e->{
            resetAllNodes();
        });
    }

    public void resetAllNodes(){
        for(VisualCircleNode currVC: visualCircles){ //update every node
            currVC.highlight(circleDropShadow);
            currVC.setHighlighted(false);
            first = true;
            unHighlightAround(currVC);
            currentInput.clear();
        }
        dragging = false;
    }

    private void selectCurrentNode(VisualCircleNode node){
        if(!node.isHighlighted()) { //check if already entered
            if (isConnected(node)) { //check if connected to the previous node
                if(!first)
                    removeOptions();

                //perform actions on current node
                node.highlight(highLightDropShadow);
                node.setHighlighted(true);

                // set current, and highlight between the previous node and this node
                currentCircleNode = node;
                first = false;
                highlightBetween(previousCircleNode,currentCircleNode);

                //set next pervious
                previousCircleNode = currentCircleNode;

                //add this to the input
                currentInput.add(node.getData());

                //show options for next choice
                showOptions(node);
            }
        }
    }

    public boolean addNodes(String data) {
        boolean added = false;
        for (VisualCircleNode vc : visualCircles) {
            if (vc.getData().equals(data)) {
                if(!vc.isHighlighted()) {
                    ArrayList<VisualCircleNode> connections = getAllConnections(vc);
                    if(connections.size()>=1||first) {
                        vc.highlight(highLightDropShadow);
                        vc.setHighlighted(true);

                        currentCircleNodes.add(vc);

                        for(VisualCircleNode vCon: connections){
                            highlightBetween(vCon,vc);
                        }

                        if (!added) {
                            currentInput.add(vc.getData());
                            added = true;
                        }

                        showOptions(vc);
                    }
                }
            }
        }
        if(!first){
            removeOptions();
        }
        if(added) {
            previousCircleNodes.clear();
            previousCircleNodes.addAll(currentCircleNodes);
            for(VisualCircleNode node: currentCircleNodes){
                //unhighlightUnused();
            }
            currentCircleNodes.clear();
        }
        first = false;
        return added;
    }

    private void initConnectorEventHandlers() {

    }

    private boolean isConnected(VisualCircleNode node){
        if(first){
            return true;
        }

        if(node.getTopConnector()!=null) {
            if (node.getTopConnector().getTopCirc() == previousCircleNode)
                return true;
        }

        if (node.getRightConnector()!=null) {
            if (node.getRightConnector().getRightCirc() == previousCircleNode)
                return true;
        }

        if (node.getBottomConnector()!=null) {
            if (node.getBottomConnector().getBottomCirc() == previousCircleNode)
                return true;
        }

        if (node.getLeftConnector()!=null) {
            if (node.getLeftConnector().getLeftCirc() == previousCircleNode)
                return true;
        }

        return false;
    }

    private ArrayList<VisualCircleNode> getAllConnections(VisualCircleNode node){
        ArrayList<VisualCircleNode> currList = new ArrayList<>();

        for(VisualCircleNode vs: previousCircleNodes){
            previousCircleNode = vs;
            if(isConnected(node))
                currList.add(vs);
        }

        return currList;
    }


    private void showOptions(VisualCircleNode node) {
        if (node.getTopConnector() != null)
            if (!node.getTopConnector().getTopCirc().isHighlighted()) {
                node.getTopConnector().highlight();
                previousConnections.push(node.getTopConnector());
            }

        if (node.getRightConnector() != null)
            if (!node.getRightConnector().getRightCirc().isHighlighted()){
                node.getRightConnector().highlight();
                previousConnections.push(node.getRightConnector());
            }

        if(node.getBottomConnector()!=null)
            if(!node.getBottomConnector().getBottomCirc().isHighlighted()) {
                node.getBottomConnector().highlight();
                previousConnections.push(node.getBottomConnector());
            }

        if(node.getLeftConnector()!=null)
            if(!node.getLeftConnector().getLeftCirc().isHighlighted()) {
                node.getLeftConnector().highlight();
                previousConnections.push(node.getLeftConnector());
            }
    }

    private void removeOptions(){
        while(!previousConnections.isEmpty()){
            previousConnections.pop().unHighlight();
        }
    }

    private void unHighlightAround(VisualCircleNode node){
        if(node.getTopConnector()!=null) node.getTopConnector().unHighlight();
        if(node.getRightConnector()!=null) node.getRightConnector().unHighlight();
        if(node.getBottomConnector()!=null) node.getBottomConnector().unHighlight();
        if(node.getLeftConnector()!=null) node.getLeftConnector().unHighlight();
    }

    private void highlightBetween(VisualCircleNode node1, VisualCircleNode node2){
        if(node1 == null)
            return;

        if(node2.getTopConnector()!=null)
            if(node2.getTopConnector().getTopCirc()==node1) {
                node2.getTopConnector().semiHighlight();
            }else{
                node2.getTopConnector().unHighlight();
            }

        if(node2.getRightConnector()!=null)
            if(node2.getRightConnector().getRightCirc()==node1) {
                node2.getRightConnector().semiHighlight();
            }else{
                node2.getRightConnector().unHighlight();
            }

        if(node2.getBottomConnector()!=null)
            if(node2.getBottomConnector().getBottomCirc()==node1) {
                node2.getBottomConnector().semiHighlight();
            }else{
                node2.getBottomConnector().unHighlight();
            }

        if(node2.getLeftConnector()!=null)
            if(node2.getLeftConnector().getLeftCirc()==node1) {
                node2.getLeftConnector().semiHighlight();
            }else{
                node2.getLeftConnector().unHighlight();
            }
    }

    private boolean isNull(VisualCircleNode node){
        return false;
    }

    public void lock(String data) {
        for(VisualCircleNode VC: visualCircles){
            String currentData = VC.getData();
            if(currentData.equals(data)){
                VC.lockNode();
                if(dataAndLockedState.containsKey(currentData))
                    dataAndLockedState.remove(currentData);
                dataAndLockedState.put(currentData,true);
                break;
            }
        }
    }

    public void unlock(String data){
        for(VisualCircleNode VC: visualCircles){
            String currentData = VC.getData();
            if(currentData.equals(data)){
                VC.unlockNode();
                if(dataAndLockedState.containsKey(currentData))
                    dataAndLockedState.remove(currentData);
                dataAndLockedState.put(currentData,false);
                break;
            }
        }
    }

    public void hideAll(){
        for(VisualCircleNode VC: visualCircles){
            VC.lockNode();
        }
    }

    public void showAll(){
        for(VisualCircleNode VC: visualCircles){
            VC.unlockNode();
        }
    }

    public void updateGrid(DataGrid dataGrid){
        this.data = dataGrid;
        initGrid();
    }

    public void showNoHighlight() {
        for(VisualCircleNode VC:currentCircleNodes){
            Circle aL = VC.getActionListner();
            aL.setOnMouseClicked(null);
            aL.setOnMouseEntered(null);
            aL.setOnMouseExited(null);
            aL.setOnMousePressed(null);
            aL.setOnMouseReleased(null);
            aL.setOnDragDetected(null);
            aL.setOnDragDetected(null);
            aL.setOnMouseEntered(null);
        }
    }

    public void setDraggable(boolean draggable){
        this.draggable = draggable;
    }

    public void clearCurrentInput(){
        currentInput.clear();
    }

    public GridPane getGrid(){
        return grid;
    }

    public ArrayList<String> getCurrentInput(){
        return currentInput;
    }

    public boolean isLocked(String data) {
        return dataAndLockedState.get(data);
    }
}
