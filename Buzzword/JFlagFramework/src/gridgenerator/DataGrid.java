package gridgenerator;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.TooManyListenersException;

/**
 * Created by Jack on 11/12/2016.
 */
public class DataGrid {
    private static final String INVALID_GRID_INPUT = "The list and col+row inputed does not match";

    private static final String TOP = "TOP";
    private static final String BOTTOM = "BOTTOM";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";

    private ArrayList<String>                nonGridItems;   //list of items before making grid
    private ArrayList<DataGridNode>          gridNodes;      //nodes in grid, used for setting up grid
    private DataGridNode                     root;           //top left corner of grid
    private int                              col;            //num of columns of grid
    private int                              row;            //num of rows of grid

    public DataGrid(ArrayList<String> nonGridItems, int col, int row){
        this.nonGridItems = nonGridItems;
        this.col = col;
        this.row = row;
        if(nonGridItems.size()==col*row && nonGridItems.size()>0)
            setUpDataGrid();
        else
            throw new InputMismatchException(INVALID_GRID_INPUT);
    }

    public DataGrid(int col, int row){
        ArrayList<String> blankList = new ArrayList<>();
        while(blankList.size()<col*row){
            blankList.add("");
        }
        this.nonGridItems = blankList;
        this.col = col;
        this.row = row;

        if(nonGridItems.size()==col*row && nonGridItems.size()>0)
            setUpDataGrid();
        else
            throw new InputMismatchException(INVALID_GRID_INPUT);
    }

    private void setUpDataGrid(){
                        root         = new DataGridNode(nonGridItems.get(0));
        int             currentRow   = 0;
        DataGridNode previousNode    = root;

        gridNodes = new ArrayList<>();

        gridNodes.add(root);

        for(int currentIndex = 1; currentIndex<nonGridItems.size(); currentIndex++){
            DataGridNode currentNode = new DataGridNode(nonGridItems.get(currentIndex));

            if(currentIndex % col == 0){ //checks if currently at the beginning column of new row
                currentRow++;
            }else { //not first node means the node has a left
                currentNode.setLeft(previousNode);
                previousNode.setRight(currentNode);
            }

            gridNodes.add(currentNode); //add node to created nodes

            if(currentRow > 0){ //checks if in second row or higher
                //gets previous top node, sets top and bottom of respective nodes
                DataGridNode currentTopNode = gridNodes.get(currentIndex-col);
                currentNode.setTop(currentTopNode);
                currentTopNode.setBottom(currentNode);
            }

            previousNode = currentNode;
        }
    }

    private ArrayList<DataGridNode> findDataGridNode(String data){
        ArrayList<DataGridNode> foundNodes = new ArrayList<>();
        for(DataGridNode node: gridNodes){
            if(node.getData().equals(data))
                foundNodes.add(node);
        }
        return foundNodes;
    }

    public boolean randomlyAdd(String word){
        int startIndex = new Random().nextInt(gridNodes.size());
        DataGridNode startNode = gridNodes.get(startIndex);

        boolean notAdded = true;
        while(notAdded) {
            if(startNode.getData().equals("")) {
                startNode.setData(Character.toString(word.charAt(0)));
                notAdded = false;
            }else{
                startIndex = new Random().nextInt(gridNodes.size());
                startNode = gridNodes.get(startIndex);
            }
        }

        if(randomAddHelper(startNode,word.substring(1))){
            return true;
        }else{
            return false;
        }
    }

    private boolean randomAddHelper(DataGridNode currentNode, String currentSubstring){
        int lengthOfSubstring = currentSubstring.length();
        if(lengthOfSubstring==0)
            return true;

        String currentLetter = Character.toString(currentSubstring.charAt(0));
        //int maxLength = getMaxAvailableLength(currentNode);

        ArrayList<String> possibleDirections = getPossibleDirections(currentNode,currentLetter);

        if(possibleDirections.size() == 0)
            return false;

        int randomIndex = new Random().nextInt(possibleDirections.size());

        String direction = possibleDirections.get(randomIndex);
        if(direction.equals(TOP))
            currentNode = currentNode.getTop();
        else if(direction.equals(BOTTOM))
            currentNode = currentNode.getBottom();
        else if(direction.equals(LEFT))
            currentNode  = currentNode.getLeft();
        else currentNode = currentNode.getRight();

        currentNode.setData(currentLetter);

        return randomAddHelper(currentNode,currentSubstring.substring(1));
    }

    private boolean isOpen(DataGridNode currentNode){
        if(currentNode!=null)
            if(currentNode.getData().equals(""))
                return true;
        return false;
    }

    private ArrayList<String> getPossibleDirections(DataGridNode currentNode, String currentLetter){
        ArrayList<String> possibleDirections = new ArrayList<>();

        if(isOpen(currentNode.getTop())){
            possibleDirections.add(TOP);
        }
        if(isOpen(currentNode.getBottom())){
            possibleDirections.add(BOTTOM);
        }
        if(isOpen(currentNode.getRight())){
            possibleDirections.add(RIGHT);
        }
        if(isOpen(currentNode.getLeft())){
            possibleDirections.add(LEFT);
        }

        return possibleDirections;
    }

    public void randomlyFillEmpty(){
        for(DataGridNode node: gridNodes){
            if(node.getData().equals("")){
                node.setData(randomLetter());
            }
        }
    }

    private String randomLetter(){
        Random r = new Random();
        char c = (char) (r.nextInt(26) + 'a');
        return Character.toString(c);
    }

    public boolean contains(String word){
        ArrayList<DataGridNode> startingNodes = findDataGridNode(word.substring(0,1));

        if(startingNodes.size()==0)
            return false;

        for(DataGridNode dgn: startingNodes){
            return isInGrid(null,dgn,word.substring(1));
        }

        return false;
    }

    private boolean isInGrid(DataGridNode prevNode, DataGridNode node, String remainingLetters){
        if(remainingLetters.length() == 0)
            return true;

        if(node.getTop()!=null)
            if(node.getTop().getData().equals(remainingLetters.substring(0,1)))
                if(node.getTop()!=prevNode)
                    if(isInGrid(node,node.getTop(),remainingLetters.substring(1)))
                        return true;

        if(node.getRight()!=null)
            if(node.getRight().getData().equals(remainingLetters.substring(0,1)))
                if(node.getRight()!=prevNode)
                    if(isInGrid(node,node.getRight(),remainingLetters.substring(1)))
                        return true;

        if(node.getBottom()!=null)
            if(node.getBottom().getData().equals(remainingLetters.substring(0,1)))
                if(node.getBottom()!=prevNode)
                    if(isInGrid(node,node.getBottom(),remainingLetters.substring(1)))
                        return true;

        if(node.getLeft()!=null)
            if(node.getLeft().getData().equals(remainingLetters.substring(0,1)))
                if(node.getLeft()!=prevNode)
                    if(isInGrid(node,node.getLeft(),remainingLetters.substring(1)))
                        return true;

        return false;
    }

    public DataGridNode getRoot(){
        return root;
    }

    public int getRow(){
        return row;
    }

    public int getCol(){
        return col;
    }

    public String getData(int col, int row){
        return nonGridItems.get(col*row-1);
    }

    public String getData(int index){
        return gridNodes.get(index).getData();
    }
}
