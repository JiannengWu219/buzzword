package gridgenerator;

import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Created by Jack on 12/9/2016.
 */
public class VisualConnectorNode {

    //COLORS AND SUCH
    private final static String HORIZONTAL_CONNECTOR = "HORIZONTAL_CONNECTOR";
    private final static Color HIGHLIGHT_COLOR       = Color.rgb(0,255,75);
    private final static Color RECTANGLE_COLOR       = Color.rgb(20,20,20);

    //shadows
    private DropShadow regularDropShadow;
    private DropShadow highLightDropShadow;
    private DropShadow semiHighlightDrowShadow;


    //SIZE OF CONNECTOR INFO
    private double radius;
    private String connectorType;
    private Rectangle actionListenerRect;
    private Rectangle backgroundRectangle;

    //THE CONNECTOR ITSELF
    private StackPane connector;

    //CONNECTIONS TO OTHER NODES
    private VisualCircleNode topCirc,rightCirc,bottomCirc,leftCirc;

    public VisualConnectorNode(double radius, String connectorType){
        this.radius        = radius;
        this.connectorType = connectorType;

        initDropShadows();
        createConnector();
        //initConnections();
    }

    private void createConnector() {
        Rectangle horRect;

        if (connectorType.equals(HORIZONTAL_CONNECTOR)) {
            actionListenerRect  = new Rectangle(radius, radius / 3);
            backgroundRectangle = new Rectangle(radius * .8, radius / 6);
        }else{
            actionListenerRect  = new Rectangle(radius / 3,radius);
            backgroundRectangle = new Rectangle(radius / 6,radius * .8);
        }

        actionListenerRect.setFill(Color.TRANSPARENT);
        actionListenerRect.setArcWidth(radius/3);
        actionListenerRect.setArcHeight(radius/3);

        backgroundRectangle.setArcHeight(radius/3);
        backgroundRectangle.setArcWidth(radius/3);
        backgroundRectangle.setFill(RECTANGLE_COLOR);
        backgroundRectangle.setEffect(regularDropShadow);

        connector = new StackPane();
        connector.getChildren().addAll(backgroundRectangle,actionListenerRect);
    }

    private void initDropShadows(){
        regularDropShadow = new DropShadow();
        regularDropShadow.setRadius(radius/5);
        regularDropShadow.setOffsetX(0);
        regularDropShadow.setOffsetY(0);
        regularDropShadow.setColor(Color.rgb(0,0,0,0.5));

        semiHighlightDrowShadow = new DropShadow();
        semiHighlightDrowShadow.setRadius(radius/3);
        semiHighlightDrowShadow.setOffsetX(0);
        semiHighlightDrowShadow.setOffsetY(0);
        semiHighlightDrowShadow.setColor(Color.rgb(120,255,120));

        highLightDropShadow = new DropShadow();
        highLightDropShadow.setRadius(radius/3);
        highLightDropShadow.setOffsetX(0);
        highLightDropShadow.setOffsetY(0);
        highLightDropShadow.setColor(Color.rgb(0,255,75,0.8));
    }

    private void initConnections() {
        this.topCirc = null;
        this.rightCirc = null;
        this.bottomCirc = null;
        this.leftCirc = null;
    }

    public void semiHighlight(){
        backgroundRectangle.setEffect(semiHighlightDrowShadow);
    }

    public void highlight(){
        backgroundRectangle.setEffect(highLightDropShadow);
    }

    public void unHighlight(){
        backgroundRectangle.setEffect(regularDropShadow);
    }

    public StackPane getNode(){
        return connector;
    }

    public void setTopCirc(VisualCircleNode circ){this.topCirc = circ;}

    public void setRightCirc(VisualCircleNode circ){this.rightCirc = circ;}

    public void setBottomCirc(VisualCircleNode circ){this.bottomCirc = circ;}

    public void setLeftCirc(VisualCircleNode circ){this.leftCirc = circ;}

    public VisualCircleNode getTopCirc(){return topCirc;}

    public VisualCircleNode getRightCirc(){return rightCirc;}

    public VisualCircleNode getBottomCirc(){return bottomCirc;}

    public VisualCircleNode getLeftCirc(){return leftCirc;}
}
