package gridgenerator;

/**
 * Created by Jack on 11/12/2016.
 */
public class DataGridNode {
    private String          data;
    private DataGridNode    top;
    private DataGridNode    right;
    private DataGridNode    bottom;
    private DataGridNode    left;

    public DataGridNode(String data){
        this.data = data;
    }

    public DataGridNode(String data, DataGridNode top, DataGridNode right, DataGridNode bottom, DataGridNode left){
        this.data   = data;
        this.top    = top;
        this.right  = right;
        this.bottom = bottom;
        this.left   = left;
    }

    /*
    Setters
     */
    public void setTop(DataGridNode top){
        this.top = top;
    }

    public void setRight(DataGridNode right){
        this.right = right;
    }

    public void setBottom(DataGridNode bottom){
        this.bottom = bottom;
    }

    public void setLeft(DataGridNode left){
        this.left = left;
    }

    public void setData(String data){
        this.data = data;
    }

    /*
    getters
     */
    public DataGridNode getTop(){
        return top;
    }

    public DataGridNode getRight(){
        return right;
    }

    public DataGridNode getBottom(){
        return bottom;
    }

    public DataGridNode getLeft(){
        return left;
    }

    public String getData(){
        return data;
    }
}
