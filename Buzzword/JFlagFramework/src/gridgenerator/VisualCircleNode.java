package gridgenerator;

import javafx.scene.control.PasswordField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Jack on 12/9/2016.
 */
public class VisualCircleNode {

    //FONTS AND COLORS
    private final static String FONT               = "Tahoma";
    private final static Color  TEXT_COLOR         = Color.rgb(0,153,92);

    private final static Color  DATA_CIRCLE_COLOR  = Color.rgb(40, 40, 40);
    private final static double INNER_CIRCLE_RATIO = 0.95;

    //shadows
    private DropShadow circleDropShadow;
    private DropShadow highLightDropShadow;
    private DropShadow semiHighlight;

    //Circle Creational Info
    private String    data;
    private double    radius;
    private Circle    dataBackgroundCircle;
    private Text      dataInNode;
    private Circle    actionListenerCircle;
    private boolean   locked;
    private boolean   highlighted;

    //THE CIRCLE ITSELF
    private StackPane circleNode;

    //CONNECTIONS TO OTHER OBJECTS
    private VisualConnectorNode topCon,rightCon,bottomCon,leftCon;

    public VisualCircleNode(double radius, String data, DropShadow regularShadow, DropShadow highlightShadow, DropShadow semiHighlight) {
        this.radius              = radius;
        this.data                = data;
        this.circleDropShadow    = regularShadow;
        this.highLightDropShadow = highlightShadow;
        this.semiHighlight       = semiHighlight;
        this.highlighted         = false;

        createCircleNode();
        //initConnections();
    }

    private void createCircleNode(){
        dataBackgroundCircle = new Circle(radius*INNER_CIRCLE_RATIO);
        dataBackgroundCircle.setFill(DATA_CIRCLE_COLOR);
        dataBackgroundCircle.setEffect(circleDropShadow);

        //Typically used for strings, change this for images
        dataInNode = new Text(data);
        dataInNode.setFont(Font.font(FONT, FontWeight.BOLD,radius*0.6));
        dataInNode.setFill(TEXT_COLOR);
        dataInNode.setBoundsType(TextBoundsType.VISUAL);

        actionListenerCircle = new Circle(radius*INNER_CIRCLE_RATIO);
        actionListenerCircle.setFill(Color.TRANSPARENT);

        //place objects into circle
        circleNode = new StackPane();
        circleNode.getChildren().addAll(dataBackgroundCircle,dataInNode,actionListenerCircle);
    }

    public void lockNode(){
        dataBackgroundCircle.setFill(Color.rgb(30,30,30));
        actionListenerCircle.setOnMouseEntered(e->{
            dataBackgroundCircle.setEffect(circleDropShadow);
        });
        locked = true;
        hideData();
    }

    public void unlockNode(){
        dataBackgroundCircle.setFill(DATA_CIRCLE_COLOR);
        actionListenerCircle.setOnMouseEntered(e->{
            dataBackgroundCircle.setEffect(semiHighlight);
        });
        locked = false;
        showData();
    }

    private void initConnections() {
        this.topCon = null;
        this.rightCon = null;
        this.bottomCon = null;
        this.leftCon = null;
    }

    public void setHighlighted(boolean highlighted){
        this.highlighted = highlighted;
    }

    public boolean isHighlighted(){
        return highlighted;
    }

    public void highlight(DropShadow type){
        dataBackgroundCircle.setEffect(type);
    }

    public void showData(){
        dataInNode.setFill(TEXT_COLOR);
    }

    public void hideData(){
        dataInNode.setFill(Color.TRANSPARENT);
    }

    public StackPane getNode(){
        return circleNode;
    }

    public Circle getActionListner(){
        return actionListenerCircle;
    }

    public String getData(){
        return data;
    }

    public void setLocked(boolean locked){
        this.locked = locked;
    }

    public boolean isLocked(){
        return locked;
    }

    public void setTopConnector(VisualConnectorNode connector){this.topCon = connector;}

    public void setRightConnector(VisualConnectorNode connector){this.rightCon = connector;}

    public void setBottomConnector(VisualConnectorNode connector){this.bottomCon = connector;}

    public void setLeftConnector(VisualConnectorNode connector){this.leftCon = connector;}

    public VisualConnectorNode getTopConnector(){return topCon;}

    public VisualConnectorNode getRightConnector(){return rightCon;}

    public VisualConnectorNode getBottomConnector(){return bottomCon;}

    public VisualConnectorNode getLeftConnector(){return leftCon;}
}
