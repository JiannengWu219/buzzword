package dynamicscoreboard;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.StackPane;

/**
 * Created by Jack on 11/12/2016.
 */
public class DynamicScoreboard {
    private TableView<WordsAndValues> table;
    private StackPane scoreboard;

    private String header1;
    private String header2;

    private ObservableList<WordsAndValues> words;

    public DynamicScoreboard(String header1, String header2){
        this.scoreboard = new StackPane();
        this.header1 = header1;
        this.header2 = header2;

        initScoreboard();
        initStyle();
    }

    private void initScoreboard(){
        table = new TableView<>();

        words = FXCollections.observableArrayList();

        table.setEditable(true);

        TableColumn wordsColumn = new TableColumn(header1);
        wordsColumn.setCellValueFactory(new PropertyValueFactory<>("word"));
        wordsColumn.setMinWidth(110);

        TableColumn valueColumn = new TableColumn(header2);
        valueColumn.setCellValueFactory(new PropertyValueFactory<>("value"));
        valueColumn.setMinWidth(60);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.setItems(words);
        table.getColumns().addAll(wordsColumn,valueColumn);

        scoreboard.getChildren().add(table);
    }

    private void initStyle(){
        String css = this.getClass().getResource("/css/dynamic_scoreboard.css").toExternalForm();
        scoreboard.getStylesheets().add(css);
        table.setId("table");
    }

    public void addWordAndValue(String word, String value){
        words.add(new WordsAndValues(word,value));
    }

    public void reset(){
        words.clear();
    }

    public StackPane getScoreboard(){
        return scoreboard;
    }

    public static class WordsAndValues{
        private final SimpleStringProperty word;
        private final SimpleStringProperty value;


        public WordsAndValues(String word, String value){
            this.word = new SimpleStringProperty(word);
            this.value = new SimpleStringProperty(value);
        }

        public String getWord(){
            return word.get();
        }

        public String getValue(){
            return value.get();
        }
    }
}
