package countdowntimer;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Formatter;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jack on 11/12/2016.
 */
public class CountdownTimer {
    private double timeRemaining;

    private Label time;
    private TimerTask timerTask;
    private Timer currentTimer;
    private StackPane timer;

    public CountdownTimer(double timeRemaining){
        this.timeRemaining = timeRemaining;
        this.timer = new StackPane();
        initTimer();
        createTimerTask();
    }

    private void initTimer(){
        currentTimer = new Timer();

        DropShadow highLightDropShadow = new DropShadow();
        highLightDropShadow.setRadius(20);
        highLightDropShadow.setOffsetX(0);
        highLightDropShadow.setOffsetY(0);
        highLightDropShadow.setColor(Color.rgb(0,0,0,0.6));

        Rectangle timerBackground = new Rectangle(145,45);
        timerBackground.setFill(Color.rgb(40,40,40));
        timerBackground.setArcWidth(20);
        timerBackground.setArcHeight(20);

        timerBackground.setEffect(highLightDropShadow);

        Label timeLeft = new Label("Time Left: ");
        time = new Label(Double.toString(timeRemaining));

        timeLeft.setFont(Font.font("Tahoma",20));
        time.setFont(Font.font("Tahoma",20));

        HBox timeLabel = new HBox();
        timeLabel.getChildren().addAll(timeLeft,time);

        timeLabel.setStyle("-fx-padding: 10 0 0 10");

        timer.getChildren().addAll(timerBackground,timeLabel);
    }

    private void createTimerTask(){
        NumberFormat formatter = new DecimalFormat("#0.00");

        timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {
                    public void run() {
                        timeRemaining-=0.05;
                        time.setText(formatter.format(timeRemaining));
                    }
                });
            }
        };
    }

    public void startTimer(){
        currentTimer = new Timer();
        createTimerTask();
        currentTimer.schedule(timerTask,50,50);
    }

    public void pauseTimer(){
        currentTimer.cancel();
    }

    public void resetTimer(int time){
        this.timeRemaining = time;
    }

    public double getTimeRemaining(){
        return timeRemaining;
    }

    public void setValue(int time){
        this.time.setText(Integer.toString(time));
    }

    public StackPane getTimer(){
        return timer;
    }
}
