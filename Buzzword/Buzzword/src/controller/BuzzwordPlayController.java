package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.GamePlayScreen;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.input.KeyEvent;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.util.ArrayList;

/**
 * Created by Jack on 12/9/2016.
 */
public class BuzzwordPlayController {

    private AppTemplate app;
    private Workspace workspace;
    private AnimationTimer timer;
    private GamePlayScreen gamePlay;
    private GameData data;
    private BuzzwordController controller;
    private String currentWord;
    private boolean pausing;

    public BuzzwordPlayController(AppTemplate app, BuzzwordController controller){
        this.app = app;
        this.controller = controller;
        pausing = true;

        initTimer();
    }

    public void initTimer(){
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                currentWord = makeWord(gamePlay.getPlayGrid().getCurrentInput());
                gamePlay.setWord(currentWord);

                if(gamePlay.getCurrentTimer().getTimeRemaining() <= 0){
                    pausing = false;
                    timer.stop();
                }
            }

            @Override
            public void stop(){
                super.stop();
                if(!pausing) {
                    endGame();
                    pausing = true;
                }
            }
        };
    }

    public void initHandlers(){
        gamePlay.getPlayGrid().getGrid().setOnMouseReleased(e->{
            checkWord();
        });

        app.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent e)->{
            if(Character.isLetter(e.getCharacter().charAt(0))){
                boolean added = gamePlay.getPlayGrid().addNodes(e.getCharacter().toLowerCase());
                if(!added){
                    gamePlay.getPlayGrid().resetAllNodes();
                }
            }else{
                checkWord();
            }
        });
    }

    private void checkWord(){
        if(data.isValid(currentWord)){
            if(!data.getUserGuessedWords().contains(currentWord)) {
                gamePlay.getScoreboard().addWordAndValue(currentWord, data.getPointValue(currentWord));
                controller.addCorrectWord(currentWord);
            }
        }
        gamePlay.getPlayGrid().resetAllNodes();
    }

    public void play(){
        initHandlers();
        timer.start();
        gamePlay.getCurrentTimer().startTimer();
    }

    public void pause(){
        stopHandlers();
        timer.stop();
        gamePlay.getCurrentTimer().pauseTimer();
    }

    public void endGame() {
        gamePlay.getCurrentTimer().pauseTimer();
        gamePlay.getCurrentTimer().setValue(0);// add all words to scoreboard
        for(String word: data.getWordsInGrid()){
            if(!data.getUserGuessedWords().contains(word)){
                gamePlay.getScoreboard().addWordAndValue(word,data.getPossibleWordsAndPoints().get(word));
            }
        }

        if(data.getCurrentScore()>=data.getTargetScore()) {
            int level = data.getLevels().get(data.getGameMode());
            if(level<=data.getCurrentLevel()) {
                data.getLevels().remove(data.getGameMode());
                data.getLevels().put(data.getGameMode(), data.getCurrentLevel()+1);
            }
            controller.saveGame();

            Platform.runLater(new Runnable() {
                public void run() {
                    controller.lockLevels();
                    promptToGoToNextLevel();
                }
            });

        }else {
            Platform.runLater(new Runnable() {
                public void run() {
                    promptToRestartLevel();
                }
            });
        }
        if(data.getCurrentScore()>data.getHighScore()){
            data.setHighScore(data.getCurrentScore());
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("New Record!","You've just broken you're previous highest score!");
                }
            });
        }
        gamePlay.getPlayGrid().showNoHighlight();
    }

    private void stopHandlers(){
        app.getGUI().getPrimaryScene().setOnKeyTyped(null);
    }

    public void initPlayScreen(){
        this.workspace = (Workspace)app.getWorkspaceComponent();
        this.data = (GameData)app.getDataComponent();
        this.gamePlay = workspace.getGamePlayClass();
    }

    private String makeWord(ArrayList<String> list){
        String word = "";
        for(int index = 0; index < list.size(); index++){
            word+= list.get(index).toUpperCase();
        }
        return word;
    }

    private void promptToGoToNextLevel(){
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        dialog.show("Congrats!", "You have beaten this level, would you like to go to the next?");
        if(dialog.getSelection().equals("Yes")){
            data.setCurrentLevel(data.getCurrentLevel()+1);
            controller.resetScreens();
            controller.startLevel(Integer.toString(data.getCurrentLevel()));
        }else
            dialog.close();
    }

    private void promptToRestartLevel(){
        YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
        dialog.show("Oh No!", "You have failed this level, would you like to restart?");
        if(dialog.getSelection().equals("Yes")){
            controller.resetScreens();
            controller.startLevel(Integer.toString(data.getCurrentLevel()));
        }else
            dialog.close();
    }
}
