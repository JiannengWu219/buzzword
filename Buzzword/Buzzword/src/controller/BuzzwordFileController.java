package controller;

import apptemplate.AppTemplate;
import data.GameData;
import data.GameDataIO;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;

import static settings.AppPropertyType.*;
import static settings.AppPropertyType.SAVE_ERROR_MESSAGE;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * Created by Jack on 11/23/2016.
 */
public class BuzzwordFileController implements FileController {

    private AppTemplate app;

    public BuzzwordFileController(AppTemplate app){
        this.app = app;
    }

    @Override
    public void handleNewRequest() {

    }

    @Override
    public boolean handleSaveRequest() {
        GameData gameData = (GameData)app.getDataComponent();
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            Path appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());

            File saveFile = new File(targetPath.toFile(),gameData.encode(gameData.getUsername())+".json");
            Files.deleteIfExists(saveFile.toPath());

            Files.createFile(saveFile.toPath());

            app.getFileComponent().saveData(app.getDataComponent(),saveFile.toPath());
            return true;
        }catch (FileAlreadyExistsException e){
            System.out.println("File already Exists: handleSaveRequest(): BuzzwordFileController");
        }catch(NoSuchAlgorithmException nae){
            nae.printStackTrace();
        }catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
        return false;
    }

    @Override
    public void handleLoadRequest() {
        GameData gameData = (GameData)app.getDataComponent();
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            Path appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());

            File loadFile = new File(targetPath.toFile(),gameData.encode(gameData.getUsername())+".json");

            app.getFileComponent().loadData(app.getDataComponent(),loadFile.toPath());
        }catch(NoSuchAlgorithmException nae){
            nae.printStackTrace();
        }catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    @Override
    public void handleExitRequest() {

    }

    public boolean checkUser(String username, String password){
        GameData gameData = (GameData)app.getDataComponent();
        PropertyManager propertyManager = PropertyManager.getManager();
        try {
            Path appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());

            File checkFile = new File(targetPath.toFile(),gameData.encode(username)+".json");

            GameDataIO gameDataIO = (GameDataIO)app.getFileComponent();
            String filePassword = gameDataIO.getPassword(checkFile.toPath());

            if(filePassword.equals(gameData.encode(password))) {
                gameData.setUsername(username);
                gameData.setPassword(password);
                return true;
            }

        }catch(NoSuchAlgorithmException nae){
            nae.printStackTrace();
        }catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(propertyManager.getPropertyValue(SAVE_ERROR_TITLE), propertyManager.getPropertyValue(SAVE_ERROR_MESSAGE));
        }

        return false;
    }
}
