package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gridgenerator.DataGrid;
import gui.GamePlayScreen;
import gui.LevelSelectScreen;
import gui.Workspace;
import ui.AppMessageDialogSingleton;
import ui.PasswordChangeMessageSingleton;
import ui.TextFieldsAndButtonsDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Formatter;

import static gui.Screens.HOME_SCREEN_LOGGED_IN;
import static gui.Screens.PLAY_SCREEN;

/**
 * Created by Jack on 11/10/2016.
 */
public class BuzzwordController{

    private AppTemplate app;
    private BuzzwordFileController fileController;
    private BuzzwordPlayController playController;
    private GameData gameData;

    public BuzzwordController(AppTemplate app){
        this.app = app;
        this.fileController = new BuzzwordFileController(app);
        this.playController = new BuzzwordPlayController(app,this);
        this.gameData = (GameData)app.getDataComponent();
    }

    public void createUser() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        String username = TextFieldsAndButtonsDialogSingleton.getSingleton().getUsername();
        String password = TextFieldsAndButtonsDialogSingleton.getSingleton().getPassword();

        //SET DATA FIRST SO WE HAVE SOMETHING TO SAVE IN CASE SAVE WORKS
        gameData.setUsername(username);
        gameData.setPassword(password);
        gameData.initData();

        if(fileController.handleSaveRequest()) {
            gameData.reset();
            workspace.resetUserAndGameInfo();
        }else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Username Already Exists", "Username already exists, please pick another.");

            boolean notCanceled = workspace.getHomeScreenLoggedOut().startRegisterPopup();
            if(notCanceled)
                createUser();
        }
    }

    public void loadUser(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        String username = TextFieldsAndButtonsDialogSingleton.getSingleton().getUsername();
        String password = TextFieldsAndButtonsDialogSingleton.getSingleton().getPassword();

        if(fileController.checkUser(username,password)){
            fileController.handleLoadRequest();
            workspace.resetUserAndGameInfo();

            updateCurrentScreen(HOME_SCREEN_LOGGED_IN.getParameter());
            workspace.updateWorkspace(HOME_SCREEN_LOGGED_IN.getParameter());
            lockLevels();

            gameData.setLoggedIn(true);
            gameData.initAllWords();
            String currentMode = gameData.getGameMode(); //get current mode
            LevelSelectScreen levelSelect = workspace.getLevelSelectClass(); //get the class
            levelSelect.setHeadingLabel(currentMode); //init the heading label of the class
        }else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("INVALID CREDENTIALS","You have entered an invalid username/password, please try again");

            boolean notCanceled = workspace.getHomeScreenLoggedOut().startLogInPopup();
            if(notCanceled)
                loadUser();
        }
    }

    public void changeMode(String mode){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        gameData.setGameMode(mode);
        gameData.initAllWords();
        workspace.getLevelSelectClass().setHeadingLabel(mode);
        workspace.resetUserAndGameInfo();
        lockLevels();
    }

    public void startLevel(String level){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getGamePlayClass().setContentLabel(gameData.getGameMode()+": "+level);
        workspace.getGamePlayClass().getCurrentTimer().resetTimer(60);
        workspace.getGamePlayClass().getScoreboard().reset();

        playController.initPlayScreen();

        gameData.initTargetWords(3+Integer.parseInt(level)/5,5+Integer.parseInt(level)/3,4+Integer.parseInt(level)/2);
        gameData.createDataGrid(5+Integer.parseInt(level)/4,5+Integer.parseInt(level)/4);
        gameData.solveGrid();
        gameData.setCurrentLevel(Integer.parseInt(level));

        NumberFormat formatter = new DecimalFormat("#0");
        Integer points = ((int)Math.round(gameData.getTotalPoints()*0.7))/10;

        gameData.setTargetScore(Math.min(points*10,Integer.parseInt(level)*75+50));
        workspace.getGamePlayClass().updateGoalScore(formatter.format(gameData.getTargetScore()));
        workspace.getGamePlayClass().updatePlayGrid(gameData.getDataGrid());

        pauseGame();

        updateCurrentScreen(PLAY_SCREEN.getParameter());
        workspace.updateWorkspace(PLAY_SCREEN.getParameter());
    }

    public void changePassword(){
        PasswordChangeMessageSingleton pws = PasswordChangeMessageSingleton.getSingleton();
        String oldPw = pws.getOldPassword();
        String newPw = pws.getNewPassword();
        if(oldPw.equals(gameData.getPassword())) {
            gameData.setPassword(newPw);
            fileController.handleSaveRequest();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Pasword Changed", "Password successfully changed.");
        }else{
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Old Password Does Not Match!","Your old password does not match the current one, password change denied.");
        }
    }

    public boolean canExit(){
        if(gameData.getCurrentScreen().equals(PLAY_SCREEN.getParameter())) {
            YesNoCancelDialogSingleton dialog = YesNoCancelDialogSingleton.getSingleton();
            dialog.show("Are you sure?", "You are in a game, are you sure you want to exit?");
            if(dialog.getSelection().equals("Yes")){
                fileController.handleSaveRequest();
                return true;
            }else
                return false;
        }else {
            fileController.handleSaveRequest();
            return true;
        }
    }

    public void resetScreens(){
        pauseGame();
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        GamePlayScreen gps= workspace.getGamePlayClass();
        gps.getScoreboard().reset();
        gps.getPlayGrid().resetAllNodes();
        gps.updateTitle(gameData.getGameMode()+" - "+gameData.getCurrentLevel());
        gps.setTotalScore("0");
        gps.getCurrentTimer().pauseTimer();
        gps.updateTimerValue("60");
        gps.clearCurrentWord();
        lockLevels();
        gameData.reset();
    }

    public void saveGame(){
        fileController.handleSaveRequest();
    }

    public void playGame(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getGamePlayClass().getPlayGrid().showAll();
        gameData.setGamePaused(false);
        playController.play();
    }

    public void pauseGame(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        workspace.getGamePlayClass().getPlayGrid().hideAll();
        workspace.getGamePlayClass().showPlayButton();
        gameData.setGamePaused(true);
        playController.pause();
    }

    public void addCorrectWord(String word){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        gameData.setCurrentScore(gameData.getCurrentScore()+Integer.parseInt(gameData.getPointValue(word)));
        gameData.getUserGuessedWords().add(word);
        workspace.getGamePlayClass().setTotalScore(Integer.toString(gameData.getCurrentScore()));
    }

    public void updateCurrentScreen(String screen) {
        gameData.setCurrentScreen(screen);
    }

    public void lockLevels(){
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        String currentMode = gameData.getGameMode(); //get current mode
        Integer currentLevel = gameData.getLevels().get(currentMode); //get current level
        LevelSelectScreen levelSelect = workspace.getLevelSelectClass(); //get the class

        for(int levelToUnlock = 1; levelToUnlock<=currentLevel; levelToUnlock++){
            levelSelect.getLevelSelectGrid().unlock(Integer.toString(levelToUnlock));
        }

        for(int levelToLock = currentLevel+1; levelToLock<=12; levelToLock++){ //lock all levels before current class
            levelSelect.getLevelSelectGrid().lock(Integer.toString(levelToLock));
        }
    }
}
