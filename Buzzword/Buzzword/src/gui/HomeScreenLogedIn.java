package gui;

import formatter.JFXFormatter;
import formatter.Positions;
import gridgenerator.DataGrid;
import gridgenerator.GridGenerator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.FourButtonsCancelDialogSingleton;
import ui.PasswordChangeMessageSingleton;
import ui.TextFieldsAndButtonsDialogSingleton;

import java.util.ArrayList;
import java.util.Collections;

import static gui.Screens.*;
import static gui.WorkspaceProperties.BOTTOM_TOOLBAR_ID;
import static gui.WorkspaceProperties.GRID_HEADING_ID;
import static gui.WorkspaceProperties.PRE_LOGIN_BUTTONS;

/**
 * Created by Jack on 11/13/2016.
 */
public class HomeScreenLogedIn {
    private static final ArrayList<String> LIST_TITLE    = new ArrayList<>();
    private static final int               CIRCLE_GAP    = 80;
    private static final int               CIRCLE_RADIUS = 65;

    private Workspace       workspace;

    private BorderPane      screen;
    private HBox            buttons;
    private HBox            content;
    private PropertyManager propertyManager;

    private Circle          settingsButton;
    private Circle          selectModeButton;
    private Circle          playButton;
    private Text            settingsText;
    private Text            selectModeText;
    private Text            playText;

    public HomeScreenLogedIn(Workspace workspace){
        propertyManager = PropertyManager.getManager();
        this.screen     = new BorderPane();
        this.workspace  = workspace;
        Collections.addAll(LIST_TITLE,"B","U","Z","Z","","","W","O","R","D");
        initScreen();
        initButtons();
        initStyle();
    }

    private void initScreen(){
        DataGrid titleGridData  = new DataGrid(LIST_TITLE,5,2);
        GridGenerator titleGrid = new GridGenerator(titleGridData,500,200);

        content = JFXFormatter.format(Positions.HORR_CENTER,titleGrid.getGrid());

        screen.setCenter(content);
    }

    private void initButtons(){
        settingsButton = new Circle(CIRCLE_RADIUS);
        settingsText   = new Text("Settings");
        settingsText.setBoundsType(TextBoundsType.VISUAL);
        StackPane settingsStackPane = new StackPane();
        settingsStackPane.getChildren().addAll(settingsButton,settingsText);

        selectModeButton = new Circle(CIRCLE_RADIUS);
        selectModeText   = new Text("Select"+"\n"+"Mode");
        selectModeText.setBoundsType(TextBoundsType.VISUAL);
        StackPane levelSelectStackPane = new StackPane();
        levelSelectStackPane.getChildren().addAll(selectModeButton,selectModeText);

        playButton = new Circle(CIRCLE_RADIUS);
        playText   = new Text("Play");
        playText.setBoundsType(TextBoundsType.VISUAL);
        StackPane playStack = new StackPane();
        playStack.getChildren().addAll(playButton,playText);

        initButtonHandlers();

        buttons = new HBox(CIRCLE_GAP);

        buttons.getChildren().addAll(JFXFormatter.getGrowBox(),playStack,levelSelectStackPane,settingsStackPane,JFXFormatter.getGrowBox());

        screen.setBottom(buttons);
    }

    //SETUP BUTTON HANDLERS
    private void initButtonHandlers(){
        playButton.setOnMouseClicked(e->{
            workspace.getGameController().updateCurrentScreen(LEVEL_SELECT_SCREEN.getParameter());
            workspace.updateWorkspace(LEVEL_SELECT_SCREEN.getParameter());
        });

        settingsButton.setOnMouseClicked(e ->{
            String option = startUserSettingsPopup();
            if(!option.equals("")){
                workspace.getGameController().changePassword();
            }
        });

        selectModeButton.setOnMouseClicked(e ->{
            String option = initSelectScreenPopup();
            if(!option.equals("")) {
                workspace.getGameController().changeMode(option);
            }
        });
    }

    public BorderPane getScreen(){
        return screen;
    }

    private void initStyle(){
        buttons.setId(propertyManager.getPropertyValue(BOTTOM_TOOLBAR_ID));
        content.setId(propertyManager.getPropertyValue(GRID_HEADING_ID));
        settingsButton.getStyleClass().setAll(propertyManager.getPropertyValue(PRE_LOGIN_BUTTONS));
        selectModeButton.getStyleClass().setAll(propertyManager.getPropertyValue(PRE_LOGIN_BUTTONS));
        playButton.getStyleClass().setAll(propertyManager.getPropertyValue(PRE_LOGIN_BUTTONS));
        settingsText.getStyleClass().setAll("text");
        selectModeText.getStyleClass().setAll("text");
        playText.getStyleClass().setAll("text");
    }

    private String startUserSettingsPopup(){
        PasswordChangeMessageSingleton pwDialog = PasswordChangeMessageSingleton.getSingleton();
        pwDialog.show("Change Password","Old Password","New Password", "Change");
        return pwDialog.getOption();
    }

    //RETURNS USER'S CHOSEN OPTION
    private String initSelectScreenPopup(){
        FourButtonsCancelDialogSingleton fourButtCanc = FourButtonsCancelDialogSingleton.getSingleton();
        fourButtCanc.show("Select Mode","Dictionary","Nouns","Adjectives","Verbs");
        return fourButtCanc.getOption();
    }
}
