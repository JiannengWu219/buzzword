package gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import propertymanager.PropertyManager;
import settings.InitializationParameters;

import java.io.File;
import java.net.URL;

import static settings.AppPropertyType.APP_CSS;
import static settings.AppPropertyType.APP_PATH_CSS;

//import static settings.InitializationParameters.ERROR_DIALOG_BUTTON_LABEL;

/**
 * This class serves to present custom text messages to the user when
 * events occur. Note that it always provides the same controls, a label
 * with a message, and a single ok button.
 *
 * @author Richard McKenna, Ritwik Banerjee
 * @author ?
 * @version 1.0
 */
public class HelpScreenPopupSingleton extends Stage {

    private static HelpScreenPopupSingleton singleton = null;

    private ScrollPane scrollPane;
    private VBox dataContainer;
    private VBox buzzwordDesc;
    private VBox levelSelectDesc;
    private VBox shortCutDesc;
    private VBox overallContainer;
    private Button cancelButton;

    private HelpScreenPopupSingleton() { }

    /**
     * A static accessor method for getting the singleton object.
     *
     * @return The one singleton dialog of this object type.
     */
    public static HelpScreenPopupSingleton getSingleton() {
        if (singleton == null)
            singleton = new HelpScreenPopupSingleton();
        return singleton;
    }


    /**
     * This function fully initializes the singleton dialog for use.
     *
     * @param owner The window above which this dialog will be centered.
     */
    public void init(Stage owner) {
        initModality(Modality.WINDOW_MODAL); // modal => messages are blocked from reaching other windows
        initOwner(owner);
        initStyle(StageStyle.TRANSPARENT);

        overallContainer = new VBox(10);
        overallContainer.setAlignment(Pos.CENTER);

        scrollPane = new ScrollPane();
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        scrollPane.setVmax(440);
        scrollPane.setPrefSize(300, 350);

        dataContainer = new VBox(30);

        buzzwordDesc = createBlurb("Buzzword"," Buzzword is a word game based off \n of creating words. You connect \n words with" +
                "either the keyboard or \n mouse and based on the word, you \n will gain points. Reach the goal in \n each level to" +
                "pass the level!");
        levelSelectDesc = createBlurb("Level Select"," As the levels increase, the " +
                "amount \n of points you need will increase. The \n length of the words will always be " +
                "at \n least 3 letters in length, and the \n maximum length will increase as the \n levels progress.");
        shortCutDesc = createBlurb("Short Cuts", "CTR + X  = Exit Game \n" +
                "CTR + I   = User Info \n" +
                "CTR + H = Help Screen ");

        dataContainer.getChildren().addAll(buzzwordDesc,levelSelectDesc,shortCutDesc);

        scrollPane.setContent(dataContainer);

        cancelButton = new Button("Cancel");
        cancelButton.setOnMouseClicked(e->{
            this.close();
        });

        overallContainer.getChildren().addAll(scrollPane,cancelButton);

        initStyles();
        Scene messageScene = new Scene(overallContainer);
        messageScene.setFill(Color.TRANSPARENT);
        this.setScene(messageScene);
    }

    private VBox createBlurb(String label, String description){
        VBox blurb = new VBox();
        Label title = new Label(label);
        title.getStyleClass().addAll("help-title");
        Text desc = new Text(description);
        desc.getStyleClass().addAll("help-desc");
        blurb.getChildren().addAll(title,desc);
        return blurb;
    }

    private void initStyles(){
        PropertyManager propertyManager = PropertyManager.getManager();
        URL cssResource = getClass().getClassLoader().getResource(propertyManager.getPropertyValue(APP_PATH_CSS) +
                File.separator +
                propertyManager.getPropertyValue(APP_CSS));
        assert cssResource != null;
        overallContainer.getStylesheets().add(cssResource.toExternalForm());

        overallContainer.setId("help-screen-popup-background");
        cancelButton.getStyleClass().addAll("cancel-button");
        scrollPane.setId("scroll-pane-in-help");
        dataContainer.setId("help-data-container");
    }

    /**
     * This method loads a custom message into the label and
     * then pops open the dialog.
     *
     * @param title   The title to appear in the dialog window.
     */
    public void show(String title) {
        showAndWait(); // opens the dialog, and waits for the user to resolve using one of the given choices
    }
}