package gui;

/**
 * Created by Jack on 11/13/2016.
 */
public enum Screens {

    LEVEL_SELECT_SCREEN("LEVEL_SELECT_SCREEN"),
    PLAY_SCREEN("PLAY_SCREEN"),
    HOME_SCREEN_LOGGED_IN("HOME_LOG_IN"),
    HOME_SCREEN_LOGGED_OUT("HOME_LOG_OUT");

    private String parameter;

    Screens(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}
