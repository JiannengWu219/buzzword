package gui;

import formatter.JFXFormatter;
import formatter.Positions;
import gridgenerator.DataGrid;
import gridgenerator.GridGenerator;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;

import java.util.ArrayList;
import java.util.Collections;

import static gui.Screens.PLAY_SCREEN;

/**
 * Created by Jack on 11/13/2016.
 */
public class LevelSelectScreen {
    private static final ArrayList<String> LEVELS    = new ArrayList<>();

    private Workspace workspace;
    private BorderPane screen;
    private HBox content;
    private PropertyManager propertyManager;
    private HBox levelSelectLabelContent;
    private Text headingLabel;
    private GridGenerator levelSelectGrid;

    public LevelSelectScreen(Workspace workspace){
        this.workspace          = workspace;
        this.propertyManager    = PropertyManager.getManager();
        this.screen             = new BorderPane();
        Collections.addAll(LEVELS,"1","2","3","4","5","6","7","8","9","10","11","12");
        initScreen();
        initStyle();
    }

    private void initScreen(){
        headingLabel = new Text();

        levelSelectLabelContent = JFXFormatter.format(Positions.HORR_CENTER,headingLabel);

        DataGrid levelGridData = new DataGrid(LEVELS,4,3);
        levelSelectGrid        = new GridGenerator(levelGridData,600,400);
        levelSelectGrid.setDraggable(false);

        content = JFXFormatter.format(Positions.HORR_CENTER,levelSelectGrid.getGrid());

        //FIXME FOR TESTING INITIALLY
        levelSelectGrid.getGrid().setOnMouseClicked(e->{
            if(levelSelectGrid.getCurrentInput().size() != 0) {
                String data = levelSelectGrid.getCurrentInput().get(0);

                levelSelectGrid.clearCurrentInput();

                if (!levelSelectGrid.isLocked(data)) {
                    workspace.getGameController().startLevel(data);
                }
            }
        });

        screen.setCenter(content);
        screen.setTop(levelSelectLabelContent);
    }

    private void initStyle(){
        headingLabel.getStyleClass().setAll("level-select-text");
        levelSelectLabelContent.getStyleClass().setAll("level-select-heading-container");
    }

    public BorderPane getScreen(){
        return screen;
    }

    public void setHeadingLabel(String heading){
        headingLabel.setText(heading);
    }

    public GridGenerator getLevelSelectGrid(){
        return levelSelectGrid;
    }
}
