package gui;

/**
 * Created by Jack on 11/13/2016.
 */
public enum WorkspaceProperties {
    ROOT_BORDER_PANE_ID,
    WORKSPACE_HEADING_LABEL,
    HEADING_LABEL,
    CANCEL_ICON_ID,

    //Homescreen logged out
    BOTTOM_TOOLBAR_ID,
    GRID_HEADING_ID,
    PRE_LOGIN_BUTTONS,
}
