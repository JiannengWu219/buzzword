package gui;

import apptemplate.AppTemplate;
import formatter.JFXFormatter;
import formatter.Positions;
import gridgenerator.DataGrid;
import gridgenerator.GridGenerator;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;
import propertymanager.PropertyManager;
import ui.TextFieldsAndButtonsDialogSingleton;

import java.util.ArrayList;
import java.util.Collections;

import static gui.WorkspaceProperties.*;
import static settings.AppPropertyType.*;

/**
 * Created by Jack on 11/13/2016.
 */
public class HomeScreenLogedOut {
    private static final ArrayList<String> LIST_TITLE    = new ArrayList<>();
    private static final int               CIRCLE_GAP    = 80;
    private static final int               CIRCLE_RADIUS = 65;

    private Workspace       workspace;
    private AppTemplate     app;

    private BorderPane      screen;
    private PropertyManager propertyManager;
    private Circle          logInButton;
    private Circle          registerButton;
    private HBox            buttons;
    private HBox            content;
    private Text            logInText;
    private Text            registerText;

    public HomeScreenLogedOut(AppTemplate app, Workspace workspace){
        propertyManager = PropertyManager.getManager();
        this.screen     = new BorderPane();
        this.app        = app;
        this.workspace  = workspace;
        Collections.addAll(LIST_TITLE,"B","U","Z","Z","","","W","O","R","D");
        initScreen();
        initButtons();
        initStyle();
    }

    private void initScreen(){
        DataGrid      titleGridData = new DataGrid(LIST_TITLE,5,2);
        GridGenerator    titleGrid     = new GridGenerator(titleGridData,500,200);

        content = JFXFormatter.format(Positions.HORR_CENTER,titleGrid.getGrid());

        screen.setCenter(content);
    }

    private void initButtons(){
        logInButton    = new Circle(CIRCLE_RADIUS);
        logInText = new Text(propertyManager.getPropertyValue(LOG_IN_TEXT));
        logInText.setBoundsType(TextBoundsType.VISUAL);
        StackPane logInStackPane = new StackPane();
        logInStackPane.getChildren().addAll(logInButton,logInText);

        registerButton    = new Circle(CIRCLE_RADIUS);
        registerText = new Text(propertyManager.getPropertyValue(REGISTER_TEXT));
        logInText.setBoundsType(TextBoundsType.VISUAL);
        StackPane registerStackPane = new StackPane();
        registerStackPane.getChildren().addAll(registerButton,registerText);

        initButtonHandlers();

        buttons = new HBox(CIRCLE_GAP);

        buttons.getChildren().addAll(JFXFormatter.getGrowBox(),logInStackPane,registerStackPane,JFXFormatter.getGrowBox());

        screen.setBottom(buttons);
    }

    private void initButtonHandlers(){
        logInButton.setOnMouseClicked(e ->{
            boolean loggedIn = startLogInPopup();
            if(loggedIn)
                workspace.getGameController().loadUser();
        });

        registerButton.setOnMouseClicked(e ->{
            boolean notCanceled = startRegisterPopup();
            if(notCanceled)
                workspace.getGameController().createUser();
        });
    }

    public BorderPane getScreen(){
        return screen;
    }

    private void initStyle(){
        buttons.setId(propertyManager.getPropertyValue(BOTTOM_TOOLBAR_ID));
        content.setId(propertyManager.getPropertyValue(GRID_HEADING_ID));
        logInButton.getStyleClass().setAll(propertyManager.getPropertyValue(PRE_LOGIN_BUTTONS));
        registerButton.getStyleClass().setAll(propertyManager.getPropertyValue(PRE_LOGIN_BUTTONS));
        logInText.getStyleClass().setAll("text");
        registerText.getStyleClass().setAll("text");
    }

    //returns successful register = true, else false
    public boolean startRegisterPopup(){
        TextFieldsAndButtonsDialogSingleton txtFldBut = TextFieldsAndButtonsDialogSingleton.getSingleton();
        txtFldBut.show("Register","Username:","Password:","Register");
        boolean cancelClicked = txtFldBut.cancelClicked();

        //IF CANCEL CLICKED, RETURN
        if(cancelClicked)
            return false;

        //ELSE SEE IF GOOD NAME/PW
        boolean isGoodUserNameAndPW = txtFldBut.isGoodPasswordAndUser();
        if(!isGoodUserNameAndPW){
            startRegisterPopup(); //IF NOT, THEN RESTART REGISTER POPUP, UNTIL USER HITS CANCEL
        }

        return isGoodUserNameAndPW; //EVENTUALLY RETURN TRUE UNLESS USER CANCELS
    }

    //returns successful login = true, else false
    public boolean startLogInPopup(){
        TextFieldsAndButtonsDialogSingleton txtFldBut = TextFieldsAndButtonsDialogSingleton.getSingleton();
        txtFldBut.show("Login","Username:","Password:","Login");
        return !txtFldBut.cancelClicked()&&txtFldBut.isGoodPasswordAndUser();
    }
}
