package gui;

import buzzword.Buzzword;
import controller.BuzzwordController;
import data.GameData;
import formatter.JFXFormatter;
import formatter.Positions;

import apptemplate.AppTemplate;
import countdowntimer.CountdownTimer;
import dynamicscoreboard.DynamicScoreboard;
import gridgenerator.DataGrid;
import gridgenerator.GridGenerator;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextBoundsType;

import javax.xml.crypto.Data;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * Created by Jack on 11/13/2016.
 */
public class GamePlayScreen {
    private static final double CONTENT_GRID_COLUMN1_PERCENT_WIDTH = 70;
    private static final double CONTENT_GRID_COLUMN2_PERCENT_WIDTH = 0; //MAKE SECOND PART OF COLUMN AS SMALL AS POSSIBLE

    private Workspace workspace;
    private BorderPane screen; //Overall content
    private AppTemplate app;

    //TOP
    private Text headingLabel;
    private HBox playLevelLabelContent;

    //MIDDLE LEFT
    private GridPane gridAndScore;

    //MIDDLE RIGHT
    private VBox        scoreAndTimerVBox;
    private HBox        guessedLetters;
    private Rectangle   gussedLetterBackground;
    private Label       totalScore;
    private VBox        scoreboardHolder;
    private StackPane   playPauseButtonHolder;
    private Rectangle   goalTextBackground;
    private Text        goalText;
    private GridGenerator titleGrid;
    private HBox gridSide;
    private HBox scoreboardAndTimerContainer;
    private HBox restartAndTimer;

    private StackPane restartButton;
    private StackPane playButton;
    private StackPane pauseButton;
    private GridPane playGoalBox;
    private DynamicScoreboard scoreboard;
    private CountdownTimer currentTimer;

    public GamePlayScreen(Workspace workspace, AppTemplate app){
        this.app          = app;
        this.workspace    = workspace;
        this.screen       = new BorderPane(); //THE PLAY SCREEN ITSELF
        initContent();
        initStyle();
    }

    private void initContent() {
        initTopOfContent();
        initMiddleContentGrid();
        initPlayGrid();
        initScoreboardAndTimerContainer(); //FOR FORMATTING PURPOSES
        initRestartButton();
        initTimer();
        initGuessedLettersBox();
        initScoreboard();
        initPlayPauseButtonAndGoalBox();
    }

    //CREATES TOP OF CONTENT
    private void initTopOfContent(){
        //top level
        headingLabel = new Text("Dictionary - 4"); //replace text when implementing data
        playLevelLabelContent = JFXFormatter.format(Positions.HORR_CENTER,headingLabel); //centers heading
        screen.setTop(playLevelLabelContent); //set heading to top of content
    }

    //CREATES AND SETS UP MIDDLE OF CONTENT GRID
    private void initMiddleContentGrid(){
        this.gridAndScore = formatPercentTwoColumns(CONTENT_GRID_COLUMN1_PERCENT_WIDTH,CONTENT_GRID_COLUMN2_PERCENT_WIDTH);
        screen.setCenter(gridAndScore);
    }

    //CREATS GRID THAT USER WILL USE TO PLAY
    private void initPlayGrid(){
        DataGrid titleGridData  = new DataGrid(5,5);
        titleGrid = new GridGenerator(titleGridData,500,500);

        gridSide = JFXFormatter.format(Positions.HORR_CENTER,titleGrid.getGrid());
        gridAndScore.add(gridSide,0,0);
    }

    //CREATES AND SETS UP THE RIGHT SIDE OF THE CONTENT GRID, FOR FORMATTING PURPOSES
    private void initScoreboardAndTimerContainer(){
        //vertical box containing scoreboard and timers
        scoreAndTimerVBox = new VBox(20);
        restartAndTimer = new HBox(10);

        scoreAndTimerVBox.getChildren().add(restartAndTimer);
        //overall box of scoreboard and timers that contains the VBox scoreboardAndTimer
        scoreboardAndTimerContainer = JFXFormatter.format(Positions.HORR_CENTER,scoreAndTimerVBox);
        gridAndScore.add(scoreboardAndTimerContainer,1,0);
    }

    private void initRestartButton() {
        restartButton = new StackPane();
        try {
            restartButton = initButton("restart.png",22.5);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

        restartAndTimer.getChildren().add(restartButton);
    }

    //CREATES THE COUNTDOWN TIMER
    private void initTimer(){
        currentTimer = new CountdownTimer(60);
        restartAndTimer.getChildren().add(currentTimer.getTimer());
    }

    //CREATES BOX FOR GUESSED LETTERS
    private void initGuessedLettersBox(){
        StackPane gussedLetterContainer = new StackPane();
        gussedLetterBackground = new Rectangle(200,45);

        guessedLetters = new HBox(5);
        gussedLetterContainer.getChildren().addAll(gussedLetterBackground,guessedLetters);
        scoreAndTimerVBox.getChildren().addAll(gussedLetterContainer);
    }

    //CREATES SCOREBOARD AND TOTAL SCORE BOX
    private void initScoreboard(){
        scoreboardHolder = new VBox();
        scoreboard = new DynamicScoreboard("Word","Value");
        totalScore = new Label("Total Score: 0");
        scoreboardHolder.getChildren().addAll(scoreboard.getScoreboard(),totalScore);
        scoreAndTimerVBox.getChildren().add(scoreboardHolder);
    }

    //CREATS THE PLAY/PAUSE BUTTON AND THE GOAL BOX IN A GRID
    private void initPlayPauseButtonAndGoalBox(){
        //play pause button with goal
        playGoalBox = formatPercentTwoColumns(40,60);
        playGoalBox.setHgap(20);
        playGoalBox.setMinWidth(200);

        //button
        try {
            playButton = initButton("play.png",30);
            pauseButton = initButton("pause.png",30);
            playPauseButtonHolder = playButton;
            playGoalBox.add(playPauseButtonHolder,0,0);
            GridPane.setHalignment(playPauseButtonHolder, HPos.LEFT);
        }catch (IOException e){
            e.printStackTrace();
        }

        initEventListener();

        //goal label
        StackPane goalTextContainer = new StackPane();
        goalTextBackground = new Rectangle(105,60);
        goalText = new Text("Goal:"+"\n"+"46 Points");
        goalText.setBoundsType(TextBoundsType.VISUAL);
        goalTextContainer.getChildren().addAll(goalTextBackground,goalText);
        playGoalBox.add(goalTextContainer,1,0);
        scoreAndTimerVBox.getChildren().add(playGoalBox);
    }

    private void initEventListener() {
        playGoalBox.setOnMouseClicked(e->{
            playGoalBox.getChildren().remove(playPauseButtonHolder);
            GameData data = (GameData) app.getDataComponent();
            if (data.getGamePaused()) {
                playPauseButtonHolder = pauseButton;
                workspace.getGameController().playGame();
            }else {
                playPauseButtonHolder = playButton;
                workspace.getGameController().pauseGame();
            }
            try {
                playGoalBox.add(playPauseButtonHolder, 0, 0);
            }catch (IllegalArgumentException iae){
                //Grid constraints non existant error
            }
        });

        restartButton.setOnMouseClicked(e->{
            GameData data = (GameData)app.getDataComponent();
            BuzzwordController controller = workspace.getGameController();
            controller.resetScreens();
            controller.startLevel(Integer.toString(data.getCurrentLevel()));
        });
    }

    private StackPane initButton(String name, double radius) throws IOException {
        StackPane exitStack = new StackPane();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(name))) {
            Image exitImage = new Image(imgInputStream);
            ImageView exitImageView = new ImageView();
            exitImageView.setImage(exitImage);

            StackPane imageHolder = new StackPane();
            imageHolder.getChildren().add(exitImageView);
            imageHolder.setPadding(new Insets(0,0,0,0)); //DONE TO CENTER THE PLAY BUTTON

            Circle playPauseButton = new Circle(radius);
            playPauseButton.getStyleClass().setAll("play-pause-circle");

            exitStack.getChildren().addAll(playPauseButton,imageHolder);

        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return exitStack;
    }

    private void initStyle(){
        gussedLetterBackground.setId("guessed-letters-background");
        guessedLetters.setId("guessed-letters-box");
        totalScore.setId("total-score-box");
        scoreboardHolder.setId("entire-score-board");
        playLevelLabelContent.getStyleClass().setAll("level-select-heading-container");
        headingLabel.getStyleClass().setAll("level-select-text");
        goalTextBackground.setId("goal-text-container");
        goalText.setId("goal-label");
    }

    private GridPane formatPercentTwoColumns(double percentWidth1, double percentWidth2){
        GridPane gridToFormat = new GridPane();

        //set percent widths
        ColumnConstraints gridColumn = new ColumnConstraints();
        gridColumn.setPercentWidth(percentWidth1);
        ColumnConstraints scoreBoardColumn = new ColumnConstraints();
        scoreBoardColumn.setPercentWidth(percentWidth2);
        gridToFormat.getColumnConstraints().addAll(gridColumn, scoreBoardColumn); // first column gets any extra width

        return gridToFormat;
    }

    public void updatePlayGrid(DataGrid currentGrid){
        gridAndScore.getChildren().clear();
        titleGrid = new GridGenerator(currentGrid,500,500);
        gridSide = JFXFormatter.format(Positions.HORR_CENTER,titleGrid.getGrid());
        gridAndScore.add(gridSide,0,0);
        gridAndScore.add(scoreboardAndTimerContainer,1,0);
    }

    public void addLetter(String letter){
        guessedLetters.getChildren().add(new Text(letter));
    }

    public void setWord(String word){
        guessedLetters.getChildren().clear();
        Text currWordToDisplay = new Text(word);
        currWordToDisplay.getStyleClass().setAll("text");
        guessedLetters.getChildren().add(currWordToDisplay);
    }

    public void clearCurrentWord(){
        guessedLetters.getChildren().clear();
    }

    public DynamicScoreboard getScoreboard(){
        return scoreboard;
    }

    public void updateTitle(String title) {
        headingLabel.setText(title);
    }

    public void updateTimerValue(String value){
        currentTimer.setValue(60);
    }

    public void updateGoalScore(String score){
        goalText.setText("Goal:"+"\n"+score);
    }

    public void showPlayButton(){
        playGoalBox.getChildren().remove(playPauseButtonHolder);
        playPauseButtonHolder = playButton;
        playGoalBox.add(playPauseButtonHolder,0,0);
    }

    public BorderPane getScreen(){
        return screen;
    }

    public void setContentLabel(String label){
        headingLabel.setText(label);
    }

    public CountdownTimer getCurrentTimer(){
        return currentTimer;
    }

    public void setTotalScore(String totalScore){
        this.totalScore.setText("Total Score: "+totalScore);
    }

    public GridGenerator getPlayGrid(){
        return titleGrid;
    }
}
