package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import data.GameData;
import formatter.JFXFormatter;
import formatter.Positions;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import ui.AppGUI;
import ui.AppMessageDialogSingleton;

import static gui.Screens.*;
import static gui.WorkspaceProperties.*;
import static settings.InitializationParameters.*;

/**
 * Created by Jack on 11/10/2016.
 */
public class Workspace extends AppWorkspaceComponent{
    private AppTemplate         app;
    private Label               guiHeadingLabel;
    private AppGUI              gui;
    private BuzzwordController  controller;

    private BorderPane          logedOutScreen;
    private BorderPane          logedInScreen;
    private BorderPane          levelSelectScreen;
    private BorderPane          playScreen;

    private PropertyManager     propertyManager;
    private VBox                userAndGameInfo;
    private HBox                logedOutTopToolBar;
    private HBox                logedInTopToolBar;
    private HBox                bottomToolBar;

    private StackPane exitButtonLoggedOut;
    private StackPane exitButtonLoggedIn;
    private StackPane homeButton;
    private StackPane helpButton;
    private StackPane logOutButton;
    private StackPane infoButton;
    private Text      welcomeUserText;
    private Text      currentGameMode;

    private HomeScreenLogedOut homeScreenLogedOut;
    private LevelSelectScreen  levelSelectClass;
    private GamePlayScreen     gamePlayClass;

    public Workspace(AppTemplate app) throws IOException{
        this.app = app;
        this.gui = app.getGUI();

        this.homeScreenLogedOut = new HomeScreenLogedOut(app,this);
        this.levelSelectClass   = new LevelSelectScreen(this);
        this.gamePlayClass      = new GamePlayScreen(this,app);

        this.logedOutScreen     = homeScreenLogedOut.getScreen();
        this.logedInScreen      = new HomeScreenLogedIn(this).getScreen();
        this.levelSelectScreen  = levelSelectClass.getScreen();
        this.playScreen         = gamePlayClass.getScreen();

        this.propertyManager   = PropertyManager.getManager();
        this.controller        = new BuzzwordController(app);

        HelpScreenPopupSingleton hsp = HelpScreenPopupSingleton.getSingleton();
        hsp.init(app.getStage());

        layoutGUI();
        initToolBars();
        initButtonHandlers();
        initUserAndGameInfo();
        initKeyBoardShortcuts();
    }

    public void layoutGUI(){
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        layoutToolBars();

        layoutUserAndGameInfoBox();

        workspace = new BorderPane();
        workspace.setTop(logedOutTopToolBar);
        workspace.setCenter(logedOutScreen);
        workspace.setBottom(bottomToolBar);
    }

    private void layoutToolBars(){
        logedOutTopToolBar = new HBox();
        logedOutTopToolBar.getChildren().add(JFXFormatter.getGrowBox());

        logedInTopToolBar = new HBox();

        bottomToolBar = new HBox();
        bottomToolBar.getChildren().add(JFXFormatter.getGrowBox());
    }

    private void layoutUserAndGameInfoBox(){
        userAndGameInfo = new VBox();

        try {
            infoButton = initButton("info.png");
            logedInTopToolBar.getChildren().add(infoButton);
        }catch (IOException e){
            e.printStackTrace();
        }

        logedInTopToolBar.getChildren().addAll(userAndGameInfo,JFXFormatter.getGrowBox());
    }

    private void initToolBars() throws IOException{
        exitButtonLoggedOut = initButton("cancel.png");
        exitButtonLoggedIn  = initButton("cancel.png");
        homeButton          = initButton("home.png");
        helpButton          = initButton("help.png");
        logOutButton        = initButton("logOut.png");

        logedOutTopToolBar.getChildren().add(exitButtonLoggedOut);
        logedInTopToolBar.getChildren().addAll(homeButton,logOutButton,exitButtonLoggedIn);
        bottomToolBar.getChildren().add(helpButton);
    }

    private void initButtonHandlers(){
        exitButtonLoggedOut.setOnMouseClicked(e->{
            gamePlayClass.getCurrentTimer().pauseTimer();
            app.getStage().close();
        });

        exitButtonLoggedIn.setOnMouseClicked(e->{
            if(controller.canExit()){
                gamePlayClass.getCurrentTimer().pauseTimer();
                app.getStage().close();
            }
        });

        homeButton.setOnMouseClicked(e->{
            if(controller.canExit()) {
                controller.updateCurrentScreen(HOME_SCREEN_LOGGED_IN.getParameter());
                updateWorkspace(HOME_SCREEN_LOGGED_IN.getParameter());
            }
        });

        helpButton.setOnMouseClicked(e->{
            HelpScreenPopupSingleton hsp = HelpScreenPopupSingleton.getSingleton();
            hsp.show();
        });

        logOutButton.setOnMouseClicked(e->{
            if(controller.canExit()) {
                ((GameData)app.getDataComponent()).setLoggedIn(false);
                controller.updateCurrentScreen(HOME_SCREEN_LOGGED_OUT.getParameter());
                updateWorkspace(HOME_SCREEN_LOGGED_OUT.getParameter());
            }
        });

        infoButton.setOnMouseClicked(e->{
            GameData data = (GameData)app.getDataComponent();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("User Info", "Your username is: "+data.getUsername()+"\n" +
                    "Your high score is: "+data.getHighScore()+"\n" +
                    "Your progress is: "+data.getLevels().toString().substring(1,data.getLevels().toString().length()-1));
        });
    }

    private StackPane initButton(String name) throws IOException{
        StackPane exitStack = new StackPane();

        URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
        if (imgDirURL == null)
            throw new FileNotFoundException("Image resources folder does not exist.");

        try (InputStream imgInputStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(name))) {
            Image exitImage = new Image(imgInputStream);
            ImageView exitImageView = new ImageView();
            exitImageView.setImage(exitImage);

            exitStack.getChildren().addAll(exitImageView);

            exitStack.getStyleClass().setAll("toolbar-button-background");
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return exitStack;
    }

    private void initUserAndGameInfo(){
        welcomeUserText = new Text("Welcome Test");
        currentGameMode = new Text("Current Mode: Test");

        userAndGameInfo.getChildren().addAll(welcomeUserText,currentGameMode);
    }

    public void resetUserAndGameInfo(){
        GameData gameData = (GameData)app.getDataComponent();
        welcomeUserText.setText("Welcome "+gameData.getUsername());
        currentGameMode.setText("Current Mode: "+gameData.getGameMode());
    }

    private void initKeyBoardShortcuts(){
        Scene ps= app.getGUI().getPrimaryScene();
        ps.getAccelerators().put( //X + control
            new KeyCodeCombination(KeyCode.X, KeyCombination.CONTROL_DOWN),
            new Runnable() {
                @Override public void run() {
                    GameData data = (GameData) app.getDataComponent();
                    if(!data.getLoggedIn())
                        app.getStage().close();
                    else
                        if(controller.canExit())
                            app.getStage().close();
                }
            }
        );

        ps.getAccelerators().put(
            new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN),
            new Runnable() {
                @Override public void run() {
                    HelpScreenPopupSingleton hsp = HelpScreenPopupSingleton.getSingleton();
                    hsp.show();
                }
            }
        );

        ps.getAccelerators().put(
            new KeyCodeCombination(KeyCode.I, KeyCombination.CONTROL_DOWN),
            new Runnable() {
                @Override public void run() {
                    GameData data = (GameData) app.getDataComponent();
                    if (data.getLoggedIn()) {
                        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                        dialog.show("User Info", "Your username is: " + data.getUsername() + "\n" +
                                "Your high score is: " + data.getHighScore() + "\n" +
                                "Your progress is: " + data.getLevels().toString().substring(1, data.getLevels().toString().length() - 1));
                    }
                }
            }
        );
    }

    public void initStyle(){
        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        workspace.setId(propertyManager.getPropertyValue(ROOT_BORDER_PANE_ID));
        logedOutTopToolBar.setId("top-tool-bar");
        userAndGameInfo.setId("top-left-box");
        welcomeUserText.getStyleClass().setAll("top-left-text");
        currentGameMode.getStyleClass().setAll("top-left-text");
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));
    }

    public void updateWorkspace(String screen){
        workspace.setTop(logedInTopToolBar);
        if(screen.equals(HOME_SCREEN_LOGGED_IN.getParameter())){
            workspace.setCenter(logedInScreen);
        }else if(screen.equals(HOME_SCREEN_LOGGED_OUT.getParameter())){
            workspace.setTop(logedOutTopToolBar);
            workspace.setCenter(logedOutScreen);
        }else if(screen.equals(LEVEL_SELECT_SCREEN.getParameter())){
            workspace.setCenter(levelSelectScreen);
        }else if(screen.equals(PLAY_SCREEN.getParameter())){
            workspace.setCenter(playScreen);
        }
    }

    public void reloadWorkspace(){
        workspace.setTop(logedOutTopToolBar);
        workspace.setCenter(logedOutScreen);
        workspace.setBottom(bottomToolBar);
    }

    public BuzzwordController getGameController() {
        return controller;
    }

    public HomeScreenLogedOut getHomeScreenLoggedOut(){
        return homeScreenLogedOut;
    }

    public LevelSelectScreen getLevelSelectClass(){ return levelSelectClass;}

    public GamePlayScreen getGamePlayClass(){
        return gamePlayClass;
    }
}
