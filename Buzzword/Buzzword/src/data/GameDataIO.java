package data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Created by Jack on 11/10/2016.
 */
public class GameDataIO implements AppFileComponent{

    public static final String PASSWORD = "PASSWORD";
    public static final String CURRENT_GAME_MODE = "CURRENT_GAME_MODE";
    public static final String LEVELS_PROGRESS = "LEVELS_PROGRESS";
    public static final String HIGH_SCORE = "HIGH_SCORE";

    @Override
    public void saveData(AppDataComponent data, Path to) {
        GameData gameData = (GameData) data;
        try {
            String password = gameData.encode(gameData.getPassword());
            String currentGameMode = gameData.getGameMode();
            HashMap<String, Integer> levelsProgress = gameData.getLevels();

            HashMap<String, Object> gameDataMap = new HashMap<>();
            gameDataMap.put(PASSWORD, password);
            gameDataMap.put(CURRENT_GAME_MODE, currentGameMode);
            gameDataMap.put(LEVELS_PROGRESS, levelsProgress);
            gameDataMap.put(HIGH_SCORE, gameData.getHighScore());

            ObjectMapper mapper = new ObjectMapper();

            mapper.writerWithDefaultPrettyPrinter().writeValue(to.toFile(), gameDataMap);
        }catch (NoSuchAlgorithmException nse){
            nse.printStackTrace();
        }catch (IOException ioe){
            ioe.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException{
        GameData gameData = (GameData) data;
        gameData.reset();

        ObjectMapper mapper = new ObjectMapper();

        HashMap<String, Object> gameDataMap;
        gameDataMap = mapper.readValue(from.toFile(), new TypeReference<HashMap<String,Object>>(){});

        String currentGameMode = (String)gameDataMap.get(CURRENT_GAME_MODE);
        int highScore = (int)gameDataMap.get(HIGH_SCORE);
        HashMap<String, Integer> levelsProgress = (HashMap<String,Integer>)gameDataMap.get(LEVELS_PROGRESS);

        gameData.setHighScore(highScore);
        gameData.setGameMode(currentGameMode);
        gameData.setLevels(levelsProgress);
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }

    public String getPassword(Path filePath) throws IOException{
        ObjectMapper mapper = new ObjectMapper();

        try {
            HashMap<String, Object> gameDataMap;
            gameDataMap = mapper.readValue(filePath.toFile(), new TypeReference<HashMap<String, Object>>() {});

            return (String)gameDataMap.get(PASSWORD);
        }catch(IOException e){
        }
        return "-1";
    }
}
