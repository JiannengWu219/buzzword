package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import gridgenerator.DataGrid;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static data.GameMode.*;
import static gui.Screens.HOME_SCREEN_LOGGED_OUT;

/**
 * Created by Jack on 11/10/2016.
 */
public class GameData implements AppDataComponent {
    private static final int TOTAL_NUMBER_OF_STORED_WORDS    = 330613;

    private AppTemplate app;

    private HashMap<String,String> possibleWordsWithPoints;
    private HashSet<String> userGuessedWords;
    private DataGrid dataGrid;
    private String username;
    private String password;
    private String gameMode;
    private String currentScreen;
    private HashMap<String, Integer> levels;
    private HashSet<String> currentDictionary;
    private HashSet<String> wordsInGrid;
    private Integer totalPoints;
    private int currentScore;
    private int currentLevel;
    private boolean gamePaused;
    private int targetScore;
    private int highScore;
    private boolean logedIn;

    public GameData(AppTemplate appTemplate) {
        app = appTemplate;
    }

    public void initData() {
        initDefaultLevels();
        initGameMode();
        initAllWords();
        initVariables();

    }

    private void initDefaultLevels() {
        levels = new HashMap<>();
        levels.put(DICTIONARY.getParameter(), 1);
        levels.put(NOUNS.getParameter(), 1);
        levels.put(ADJECTIVES.getParameter(), 1);
        levels.put(VERBS.getParameter(), 1);
    }

    public void initTargetWords(int shortestLength, int longestLength, int numWords) {
        wordsInGrid = new HashSet<>();
        userGuessedWords = new HashSet<>();

        ArrayList<String> arrayListOfAllWords = new ArrayList<>(currentDictionary);

        possibleWordsWithPoints = new HashMap<>();
        totalPoints = 0;

        int toSkip = randomize();
        int wordCounter = 0;
        while(wordCounter<numWords) {
            String currWord = arrayListOfAllWords.get(toSkip);
            if (currWord.length() < shortestLength || currWord.length() > longestLength) {
                toSkip = randomize();
                continue;
            }

            boolean isWord = true;
            for (char c : currWord.toCharArray()) {
                if (!Character.isLetter(c)) {
                    toSkip ++;
                    isWord = false;
                    break;
                }
            }

            if(isWord) {
                wordCounter++;
                wordsInGrid.add(currWord);
                toSkip = randomize();
            }
        }
    }

    public void solveGrid(){
        int wordsChecked = 0;
        for(String word: currentDictionary) {
            if(word.length()>=3) {
                if (dataGrid.contains(word)) {
                    wordsInGrid.add(word);
                }
            }
        }

        for(String word: wordsInGrid){
            getPossibleWordsAndPoints().put(word,getPointValue(word));
            totalPoints += Integer.parseInt(getPointValue(word));
        }

        System.out.println("IN DATA: WORDS FOUND: "+wordsInGrid.toString());

    }

    public void createDataGrid(int row, int height){
        dataGrid = new DataGrid(row,height);

        ArrayList<String> wordsToRemove = new ArrayList<>();

        for(String word: wordsInGrid){
            //if word is not added, remove it
            if(!dataGrid.randomlyAdd(word)){
                wordsToRemove.add(word);
            }
        }

        for(String word: wordsToRemove){
            //remove word from data
            wordsInGrid.remove(word);
        }

        dataGrid.randomlyFillEmpty();
    }

    private int randomize(){
        return new Random().nextInt(currentDictionary.size()-1);
    }

    public void initAllWords() {
        currentDictionary = new HashSet<>();
        URL wordsResource = getClass().getClassLoader().getResource("words/"+gameMode+".txt");
        assert wordsResource != null;

        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            lines.forEach(word->currentDictionary.add(word));
        } catch (IOException |
                URISyntaxException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void initVariables() {
        this.currentScreen = HOME_SCREEN_LOGGED_OUT.getParameter();
        this.gamePaused = true;
        this.currentScore = 0;
        targetScore = 0;
        currentLevel = 0;
        highScore = 0;
        logedIn =false;
    }

    public String getPointValue(String word){
        int points = 0;

        for(char c: word.toLowerCase().toCharArray()){
            if (isVowel(c))
                points += 5;
            else
                points += 10;
        }

        return Integer.toString(points);
    }

    private boolean isVowel(char c){
        return (c == 'a' || c=='e' || c=='i'|| c=='o' || c=='u');
    }

    private void initGameMode(){
        gameMode = DICTIONARY.getParameter();
    }

    public boolean isValid(String word){
        if(word.length()>=3)
            return currentDictionary.contains(word.toLowerCase());
        else
            return false;
    }

    private void setPossibleWords(){

    }

    private void createGridOfLetters(){

    }

    public void reset(){
        totalPoints = 0;
        currentScore = 0;
        targetScore = 0;
    }

    public void resetLevel(){

    }

    public String encode(String s) throws NoSuchAlgorithmException{
        MessageDigest m= MessageDigest.getInstance("MD5");
         m.update(s.getBytes(),0,s.length());
        return new BigInteger(1,m.digest()).toString(16);
    }

    public void setCurrentScreen (String screen){
        this.currentScreen = screen;
    }

    public void setGamePaused(boolean gamePaused){
        this.gamePaused = gamePaused;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setGameMode(String gameMode){
        this.gameMode = gameMode;
    }

    public void setLevels(HashMap<String,Integer> levels){
        this.levels = levels;
    }

    public String getCurrentScreen() {return currentScreen;}

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }

    public String getGameMode(){
        return gameMode;
    }

    public boolean getGamePaused(){
        return gamePaused;
    }

    public HashSet<String> getCurrentDictionary(){return currentDictionary;}

    public Integer getTotalPoints(){
        return totalPoints;
    }

    public void setTotalPoints(Integer points){
        this.totalPoints = points;
    }

    public static int getTotalNumberOfStoredWords() {
        return TOTAL_NUMBER_OF_STORED_WORDS;
    }

    public HashMap<String,String> getPossibleWordsAndPoints(){
        return possibleWordsWithPoints;
    }

    public HashMap<String,Integer> getLevels(){
        return levels;
    }

    public int getTargetScore(){
        return targetScore;
    }

    public boolean getLoggedIn(){
        return logedIn;
    }

    public void setLoggedIn(boolean logedIn){
        this.logedIn = logedIn;
    }

    public void setTargetScore(int score){
        targetScore = score;
    }

    public void setCurrentScore(int score){
        this.currentScore = score;
    }

    public int getCurrentScore(){
        return currentScore;
    }

    public DataGrid getDataGrid() {
        return dataGrid;
    }

    public HashSet<String> getUserGuessedWords(){
        return userGuessedWords;
    }

    public HashSet<String> getWordsInGrid(){
        return wordsInGrid;
    }

    public void setHighScore(int score){
        this.highScore = score;
    }

    public int getHighScore(){
        return highScore;
    }

    public void setCurrentLevel(int level){
        this.currentLevel = level;
    }

    public int getCurrentLevel(){
        return currentLevel;
    }
}
