package data;

/**
 * Created by Jack on 11/23/2016.
 */
public enum GameMode {
    DICTIONARY("Dictionary"),
    NOUNS("Nouns"),
    ADJECTIVES("Adjectives"),
    VERBS("Verbs");

    private String parameter;

    GameMode(String parameter) {
        this.parameter = parameter;
    }

    public String getParameter() {
        return parameter;
    }
}
