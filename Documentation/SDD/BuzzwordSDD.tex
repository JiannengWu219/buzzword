\documentclass[11pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{tabularx}
\usepackage[margin=0.7in]{geometry}
\graphicspath{ {images/} }

\begin{document}
\vspace{12cm}
\newcommand*{\lsim}{\mathord{\sim}}
\newcommand*{\dint}{\indent \indent}
\newcommand*{\tint}{\indent \indent \indent}
\setlength{\parindent}{0pt}

\vspace{2cm}

\resizebox{\linewidth}{!}{Buzzword\texttrademark}
\Huge
Software Design Description\\

\vspace{8cm}

\large
\bf{Author:} \normalfont{ \indent Jianneng Wu (Jack)}\\
\tint \tint Professaur Inc.\\
\tint \tint October, 2016\\
\tint \tint Version 1.0\\

\bf{Abstract:} \normalfont{\indent This document describes the software design for Buzzword, a word 
\tint \tint based educational game.\\

\bf{Based on IEEE Std 1016\texttrademark -2009 document format\\

\textcopyright 2016 Professaur Inc.\\
\normalfont \indent This document or any portion thereof may not be reproduced or used in any \indent manner whatsoever
without the express written permission of the author.

\clearpage

\textbf{1. Introduction}\\
This is the software design description for the Buzzword\texttrademark application.\\

\textbf{1.1 Purpose}\\
This document is to serve as the blueprint for the construction of the Buzzword application. This design
will use UML class diagrams to provide complete detail regarding all packages, classes, instance variables,
class variables, and method signatures needed to build the application. In addition, UML Sequence diagrams
will be used to specify object interactions post-initialization of the application, meaning in response to user interactions or timed events.\\

\textbf{1.2 Scope}\\
Buzzword will be an educational word game based on a grid interface with a scoreboard and timer. Tools
for its construction should be developed with this in mind such that additional word games may avoid
duplication of work. As such, a framework called the  \textbf{Java Framework for Language Applications and Games} (JFLAG) Framework, will be designed and constructed
along with the Buzzword game such that it may be used to build additional word games. So, this design
contains design descriptions for the development of both the framework and game. Note that Java is the target language for this software design\\ 

\textbf{1.3 Definitions, acronyms, and abbreviations}\\

\textbf{Class Diagram} - A UML document format that describes classes graphically. Specifically, it describes their
instance variables, method headers, and relationships to other classes.\\

\textbf{IEEE} - Institute of Electrical and Electronics Engineers, the “world’s largest professional association for the
advancement of technology”.\\

\textbf{Framework} - In an object-oriented language, a collection of classes and interfaces that collectively provide a
service for building applications or additional frameworks all with a common need.\\

\textbf{Java} -  A high-level programming language that uses a virtual machine layer between the Java application and
the hardware to provide program portability.\\

\textbf{JavaFX} -  JavaFX is a software platform for creating and delivering desktop applications, as well as rich internet applications (RIAs) that can run across a wide variety of devices. \\

\textbf{JFXFramework} - A framework designed and edited by Richard Mckenna and Ritwik Banerjee from Stony Brook University for making standard GUI applications.\\

\textbf{JFLAG Framework} - The software framework that will be developed along with Buzzword in order to easily construct other games with grid-based design, with a timer, or with a scoreboard.\\

\textbf{Sequence Diagram} - A UML document format that specifies how object methods interact with one another.\\

\textbf{UML} - Unified Modeling Language, a set of document formats for designing software graphically.\\

\textbf{Buzzword} - Title of the word game described by this document.\\

\clearpage

\textbf{1.4 References}\\

\textbf{IEEE Std 830\texttrademark -1998 (R2009)} - IEEE Standard for Information Technology – Systems Design – Software.\\

\textbf{Buzzword\texttrademark SRS} - Professaur Inc.'s Software Requirements Specification for the Buzzword word game application.\\ \\ \\

\textbf{1.5 Overview}\\
This Software Design Description document provides a working design for the Buzzword software
application as described in the Buzzword Software Requirements Specification. Note that all parties in the
implementation stage must agree upon all connections between components before proceeding with the
implementation stage. Section 2 of this document will provide the Package-Level Viewpoint, specifying the
packages and frameworks to be designed. Section 3 will provide the Class-Level Viewpoint, using UML Class
Diagrams to specify how the classes should be constructed. Section 4 will provide the Method-Level System
Viewpoint, describing how methods will interact with one another. Section 5 provides deployment information
like file structures and formats to use. Section 6 provides a Table of Contents, an Index, and References. Note
that all UML Diagrams in this document were created using the VioletUML editor.\\

\clearpage

\textbf{2 Package-Level Design Viewpoint}\\
As mentioned, this design will encompass both the Buzzword game application and the JFLAG Framework to be used in its construction. In building both we will heavily rely on the Java and JavaFX APIs to provide services. Following are descriptions of the components to be built, as well as how the Java API and JavaFX API will be used to build them.\\ 

\textbf{2.1 Buzzword and JFLAG Overview}\\
The Buzzword and JFLAG framework will be designed and developed in tandem. Figure 2.1 specifies all the components to be developed and places all classes in home packages.\\

\begin{tabular}{|c|c|}
\hline
JFLAG Framework & Buzzword Game \\
\includegraphics[scale=0.8]{jflag_package}&\includegraphics[scale=0.65]{buzzword_package}\\
\hline
\end{tabular}

\bigskip
\textbf{Figure 2.1: Design Packages Overview}\\

\clearpage

\textbf{2.2 JFXFramework}\\
The Buzzword game will be developed using the JFXFramework, this design will make use of the classes specified in figure 2.2\\

\includegraphics[scale=0.8]{jfx_framework_package}\\
\bigskip
\textbf{Figure 2.2} JFXFramework and packages to be used\\ \\ 

\textbf{2.3 JFXFramework Usage Descriptions}\\
Tables 2.1-2.5 below summarize how each of these classes will be used.\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
AppTemplate & Used as a reference for the application as a whole\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.1: Uses for classes in JFX.apptemplate package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
PropertyManager & Properties the AppTemplate will use in building and rendering the application\\
\hline
\end{tabularx}\\\\\
\bigskip
\textbf{Table 2.2: Uses for classes in JFX.propertymanager package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
AppFileController & Handle the applications save, load, and new requests\\
FileController & Interface for AppFileController\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.3: Uses for classes in JFX.controller package}\\

\clearpage

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
AppComponentsBuilder & Blueprint for creating core components of the app\\
AppDataComponent & Interface for any data component of the app\\
AppFileComponent & Interface for any file component of the app\\
AppWorkspaceComponent & Blueprint for the workspace of the app\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.4: Uses for classes in JFX.components package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
AppGUI & The outer GUI of the app, not including the workspace\\
AppMessageDialogSingleton & Pop-up for sending a message dialog to the user\\
YesNoCancelDialogSingleton & Pop-up for sending a yes, no, cancel dialog to user\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.5: Uses for classes in JFX.ui package}\\ \\

\clearpage

\textbf{2.4 Java and JavaFX}\\
JFLAG Framework and Buzzword will be developed using the Java programming languages. This design will make use of the following classes in figure 2.3 and 2.4.\\

\includegraphics[scale=0.8]{java_package}\\
\bigskip
\textbf{Figure 2.3} Java API and packages to be used\\

\includegraphics[scale=0.65]{javafx_package}\\
\bigskip
\textbf{Figure 2.4} JavaFX API and packages to be used\\

\clearpage
\textbf{2.5 Java and JavaFX usage description}\\
Tables 2.6-2.20 below summarize how each of these classes will be used.\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
ReentrantLock & For ensuring only one thread has access to a piece of data\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.6: Uses for classes in java.util.concurrent.locks package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
IOException & Exception for improper IO usage\\
OutputStream & For writing to a file\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.7: Uses for classes in java.io package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Files & File object for when writing and retrieving files\\
Paths & Paths for each file for writing and retrieving files\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.8: Uses for classes in java.nio.file package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
URISyntaxException & Exception for URL\\
URL & Get URL to directories\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.9: Uses for classes in java.net package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
HashSet & For storing <name, value> pairs\\
Random & Get a random number\\
Set & Parent of HashSet\\
LinkedList & Storing nodes in a linked fashion\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.10: Uses for classes in java.util package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Stream & Read text files\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.11: Uses for classes in java.util.stream package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Stage & The window for the app\\
FileChooser & Choose a file from a directory\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.12: Uses for classes in javafx.stage package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Button & Button in the app for input\\
Label & Label in the app for text\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.13: Uses for classes in javafx.scene.control package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
AnimationTimer & Timer for multiple threads\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.14: Uses for classes in javafx.animation package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Pane & General content placed with X,Y from top left corner\\
BorderPane & Content in in window placed left, right, top, bottom, or center positions\\
HBox & Content in window placed horizontally\\
VBox & Content in window placed vertically\\
StackPane & Content in window over-layed upon each other\\
FlowPane & Content in window that does not over-lay with one another\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.15: Uses for classes in javafx.layout package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Insets & Insets for spacing\\
Pos & Position of a node in the window\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.16: Uses for classes in javafx.geometry package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Text & Text in the app\\
Font & Font of the text in the app\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.17: Uses for classes in javafx.scene.text package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Scene & The content that will be shown inside the window or stage\\
Node & Objects inside the scene\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.18: Uses for classes in javafx.scene package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Platform & The app's platform that runs threads\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.19: Uses for classes in javafx.application package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
Circle & Create circles inside the scene\\
Rectangle & Create rectangles inside the scene\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.19: Uses for classes in javafx.scene.shape package}\\

\begin{tabularx}{.8\textwidth}{|c|X|}
\hline
Class/Interface & Use \\
\hline
KeyEvent & Event based on a keyboard button press\\
\hline
\end{tabularx}\\\\
\bigskip
\textbf{Table 2.20: Uses for classes in javafx.scene.input package}\\

\textbf{3 Class-Level Design Viewpoint}\\
As mentioned, this design will encompass both the Buzzword game and the JFLAG Framework. The following UML Class Diagrams reflect this. Note that due to the complexity of the project, we
present the class designs using a series of diagrams going from overview diagrams down to detailed ones.\\

\includegraphics[scale=0.35]{OVERVIEW_CLASS}\\
\bigskip
\textbf{Figure 3.1} Overview of the game\\

\includegraphics[scale=0.42]{JFLAG_Updaters}\\
\bigskip
\textbf{Figure 3.2} JFLAG Framework and it's respective updater\\

\includegraphics[scale=0.5]{Worspace_GUI}\\
\bigskip
\textbf{Figure 3.3} The workspace and GUI\\

\includegraphics[scale=0.5]{GameData_Controller}\\
\bigskip
\textbf{Figure 3.4} The game data and Controller\\

\clearpage
\textbf{4 Method-Level Design Viewpoint}\\
Now that the general architecture of the classes has been determined, it is time to specify how data will flow
through the system. The following UML Sequence Diagrams describe the methods called within the code to be
developed in order to provide the appropriate event responses. \\

\includegraphics[scale=0.85]{case1}\\
\bigskip
\textbf{Figure 4.1} Handle Create Profile\\

\includegraphics[scale=0.48]{case2}\\
\bigskip
\textbf{Figure 4.2} Handle Login/Logout\\

\includegraphics[scale=0.9]{case3}\\
\bigskip
\textbf{Figure 4.3} Handle Start Playing\\

\includegraphics[scale=0.6]{case4}\\
\bigskip
\textbf{Figure 4.4} Handle Select Level\\

\includegraphics[scale=0.9]{case5}\\
\bigskip
\textbf{Figure 4.5} Handle Select Game Mode\\

\includegraphics[scale=1]{case6}\\
\bigskip
\textbf{Figure 4.6} Handle View Help\\

\includegraphics[scale=1]{case7}\\
\bigskip
\textbf{Figure 4.7} Handle Quit Application\\

\includegraphics[scale=0.7]{case8}\\
\bigskip
\textbf{Figure 4.8} Handle Return to Home Screen\\

\includegraphics[scale=0.8]{case9}\\
\bigskip
\textbf{Figure 4.9} Handle Pause/ Resume game\\

\includegraphics[scale=0.8]{case10}\\
\bigskip
\textbf{Figure 4.10} Handle Select word by dragging\\

\includegraphics[scale=0.75]{case11}\\
\bigskip
\textbf{Figure 4.11} Handle Select word by typing\\

\includegraphics[scale=0.8]{case12}\\
\bigskip
\textbf{Figure 4.12} Handle Replay Level\\

\includegraphics[scale=0.7]{case13}\\
\bigskip
\textbf{Figure 4.13} Handle Start Next Level\\

\includegraphics[scale=0.8]{case14}\\
\bigskip
\textbf{Figure 4.14} Handle Save Progress\\

\includegraphics[scale=0.8]{case15}\\
\bigskip
\textbf{Figure 4.15} Handle Close "Personal Best" Dialog\\

\includegraphics[scale=0.7]{case16}\\
\bigskip
\textbf{Figure 4.16} Handle Scroll Through Help\\

\includegraphics[scale=1]{case18}\\
\bigskip
\textbf{Figure 4.17} Level Gameplay Ends\\

\clearpage
\textbf{5 File Structure and Formats}\\
The JFXFramework will be provided as a directory from past work. It will utilize the files of the XML File format for initializing it's core functions. The files should be located inside a resources directory inside the Buzzword directory.\\

\textbf{5.1 Buzzword File Structure and Formats}\\
The Buzzword game will utilize files of the JSON format to load the user info including the user's progress, name, and password. There will be many JSON files, specifically, the number of users +1 JSON files. There will be one JSON file for each user, and one global one to check for user validity. This assures quick loading of the user once verified.\\ \\ \\

\textbf{6 Supporting Information}\\

Note that this document should serve as a reference for those implementing the code, so we’ll provide a table of
contents to help quickly find important sections.\\ \\ 

\textbf{6.1 Table of Contents}\\ \\
\begin{tabular}{p{10cm}|c}
1. Introduction & 2\\
\hspace{10mm} 1. Purpose & 2\\
\hspace{10mm} 2. Scope & 2\\
\hspace{10mm} 3. Definitions, acronyms, and abbreviations & 2\\
\hspace{10mm} 4. References & 3\\
\hspace{10mm} 5. Overview & 3\\
2. Package-Level Design Viewpoint & 4\\
\hspace{10mm} 1. Buzzword and JFLAG Overview & 5\\
\hspace{10mm} 2. JFXFramework & 5\\
\hspace{10mm} 3. JFXFramework Usage Descriptions & 5\\
\hspace{10mm} 4. Java and JavaFX &7\\
\hspace{10mm} 5. Java and JavaFX usage Descriptions & 8\\
3. Class-Level Design Viewpoint& 10\\
4. Method-Level Design Viewpoint& 13\\
5. File Structure and Formats & 23\\
\hspace{10mm} 1. Buzzword File Structure and Formats & 23\\
6. Supporting Information & 24\\
\hspace{10mm} 1. Table of Contents & 24\\
\hspace{10mm} 2. Appendixes  & 24\\
\end{tabular}\\ \\ \\

\textbf{6.2 Appendixes}\\ \\
N/A

\end{document}